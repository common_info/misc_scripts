from __future__ import annotations

import os
import sys
from functools import wraps, update_wrapper
from pathlib import Path
from types import CodeType, FunctionType
from typing import Optional, Any, Iterable, Mapping, Union

from loguru import logger

from custom_user_input import multiline_iterable_input
from init_logger import configure_custom_logging
from json_parse import BaseJSONParser, JSONElementTree

_default_interceptors: tuple[str, ...] = ("__exit__", "exit", "__quit__", "quit", "__stop__", "stop", "e", "q", "s")
PathLike = Union[str, Path]


class CommandNameNotFound(KeyError):
    """The command with the name is not found."""


class CommandNameTypeError(TypeError):
    """The command name has an invalid type."""


class CommandTypeError(TypeError):
    """The command has an invalid type."""


class CommandArgumentNameError(KeyError):
    """The command does not have a field with such name."""


class CommandList:
    _commands: dict[str, Command] = dict()
    _intercept_commands: list[str] = []

    def __init__(self, commands: Mapping[str, Command] = None, *, intercept_commands: Iterable[str] = None):
        if commands is None:
            commands: dict[str, Command] = dict()
        if intercept_commands is None:
            intercept_commands: list[str] = []
        self.__class__._commands.update(**commands)
        self.__class__._intercept_commands.extend(intercept_commands)

    def __str__(self):
        _cmd_names: str = ", ".join(self._commands.keys())
        return f"{self.__class__.__name__}: {_cmd_names}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._commands.items()})>"

    def __getitem__(self, item, *args, **kwargs):
        if isinstance(item, str):
            if item in self._commands:
                return self._commands.get(item).__call__(*args, **kwargs)
            else:
                logger.error(f"The command {item} is not found")
                raise CommandNameNotFound
        else:
            logger.error(f"The item {item} of type {type(item)} is not found")
            raise KeyError

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            logger.error(f"The name of the command must be of the str type, but {type(key)} received")
            raise CommandNameTypeError
        if not isinstance(value, Command):
            logger.error(f"The command must be of the Command type, but {type(value)} received")
            raise CommandTypeError
        self._commands.__setitem__(key, value)

    def get_command(self, command_name: str, default: Command | None = None):
        if command_name not in self._commands:
            logger.error(f"The command {command_name} is not found")
            return default
        for command in self._commands.values():
            if command.name == command_name:
                return command

    # @classmethod
    # def __call__(cls, *args, **kwargs):
    #     return cls._instance

    def __delitem__(self, key):
        if key in self._commands:
            return self._commands.__delitem__(key)
        else:
            logger.error(f"The command {key} is not found")

    def __iter__(self):
        return iter(self._commands.keys())

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._commands.keys()
        elif isinstance(item, Command):
            return item in self._commands.values()
        else:
            return False

    def __len__(self):
        return len(self._commands)

    def keys(self):
        return self._commands.keys()

    def values(self):
        return self._commands.values()

    def items(self):
        return self._commands.items()

    def __add__(self, other):
        if isinstance(other, Command):
            self.__setitem__(other.func.__name__, other)
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def __sub__(self, other):
        if isinstance(other, Command):
            self.__delitem__(other.name)
        else:
            return NotImplemented

    @property
    def commands(self):
        return self._commands

    @commands.setter
    def commands(self, value):
        self.__class__._commands = value

    @property
    def intercept_commands(self):
        return self._intercept_commands

    @intercept_commands.setter
    def intercept_commands(self, value):
        self.__class__._intercept_commands = value

    def add_intercept_command(self, command_name: str):
        if command_name in self._intercept_commands:
            logger.info(f"The interception command {command_name} is already listed")
        else:
            self._intercept_commands.append(command_name)

    def delete_intercept_command(self, command_name: str):
        if command_name not in self._intercept_commands:
            logger.info(f"The interception command {command_name} is not found")
        else:
            self._intercept_commands.remove(command_name)


class Command:
    def __init__(self, func: FunctionType, command_list: CommandList):
        self._func: FunctionType = func
        self._name: str = self._func.__name__
        self._command_list: CommandList = command_list

    @property
    def code(self) -> CodeType:
        return self._func.__code__

    def __call__(self, *args, **kwargs):
        return self._func(*args, **kwargs)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.name})>"

    @property
    def name(self):
        return self.code.co_name

    # @name.setter
    # def name(self, value):
    #     self._name = value

    @property
    def func(self):
        return self._func

    @func.setter
    def func(self, value):
        self._func = value

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._func == other._func
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._func != other._func
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, CommandList):
            return other.__add__(self)
        else:
            return NotImplemented

    __radd__ = __add__

    def __sub__(self, other):
        if isinstance(other, CommandList):
            return other.__sub__(self)
        else:
            return NotImplemented

    @property
    def _posargcount(self) -> int:
        return self.code.co_argcount

    @property
    def _kwargcount(self) -> int:
        return self.code.co_kwonlyargcount

    @property
    def _arg_count(self) -> int:
        return self._posargcount + self._kwargcount

    @property
    def varnames(self) -> tuple[str, ...]:
        return self.code.co_varnames[:self._arg_count]

    @property
    def args(self) -> tuple[str, ...]:
        return self.varnames[:self._posargcount + 1]

    @property
    def kwargs(self) -> tuple[str, ...]:
        return self.varnames[self._posargcount + 1:]


def command_api(command_list: CommandList):
    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            # _command: Command = Command(func(*args, **kwargs), command_list)
            return func(*args, **kwargs)
        # inner.__name__ = func.__name__
        return Command(inner, command_list)
    # wrapper.__name__ = fun.__name__
    return wrapper


class UserInputParser:
    def __init__(self, command_list: Optional[CommandList] = None):
        if command_list is None:
            command_list: CommandList = CommandList()
        self._command_list: CommandList = command_list
        self._input: Optional[str] = None
        self._command: Optional[Command] = None
        self.pos_args: Optional[list[Any]] = []
        self.kw_args: Optional[dict[str, Any]] = {}

    @property
    def command_list(self):
        return self._command_list

    @command_list.setter
    def command_list(self, value):
        self._command_list = value

    @property
    def input(self):
        return self._input

    @input.setter
    def input(self, value):
        self._input = value

    @property
    def command(self):
        return self._command

    @command.setter
    def command(self, value):
        self._command = value

    def _validate(self):
        if self._input is not None and self._input.lower() in self._command_list.intercept_commands:
            sys.exit(-1)
        return

    def get_input(self, prompt: str):
        self._input = input(prompt).strip()
        self._validate()

    def _split(self):
        return self._input.split()

    def __iter__(self):
        return iter(self._split()[1:])

    def __getitem__(self, item):
        return self._split().__getitem__(item)

    def _get_command(self):
        _command_name: str = self._split().__getitem__(0).lower()
        logger.info(f"_command_name = {_command_name}")
        logger.info(f"{self._command_list.commands.keys()}")
        logger.info(f"{self._command_list.get_command(_command_name)}")
        logger.info(self._command_list.get_command("split"))
        if _command_name not in self._command_list:
            logger.error(f"The command {_command_name} is not found")
            raise CommandNameNotFound
        self._command = self._command_list.get_command(_command_name)

    @property
    def _args(self) -> list[str]:
        return list(filter(lambda x: not x.startswith("--"), self))

    @property
    def _kwargs(self) -> list[str]:
        return list(filter(lambda x: x.startswith("--"), self))

    def _get_args(self):
        for arg in self._args:
            self.pos_args.append(arg)

    def _get_kwargs(self):
        for kwarg in self._kwargs:
            kwarg_name, kwarg_value = kwarg.strip("--").split("=")
            self.kw_args[kwarg_name] = kwarg_value

    def prepare(self):
        self._get_command()
        self._get_args()
        self._get_kwargs()
        return self._command.name


@command_api
def print_hello(text: str, *, value: str = ""):
    print(text)
    print(value)
    print(f"{text} = {value}")
    return


# @command_api
def split(line: str, *, separator: str = None):
    logger.info(f"Split value:\n{tuple(iter(line.split(separator)))}")
    return


# @command_api
def join_values(symbol: str, values: Iterable[str]):
    logger.info(f"Joined value:\n{symbol.join(values)}")
    return


# @command_api
def recursive_iterdir(path: PathLike) -> list[str]:
    return [os.path.join(root, file) for root, _, files in os.walk(path) for file in files]


# @command_api
def stop():
    logger.info("You stop the script. Good bye.")
    sys.exit(-1)


# @command_api
def json_parse(sentinel: str = None):
    _ = multiline_iterable_input(sentinel)
    json_lines = "\n".join(_)

    base_json_parser: BaseJSONParser = BaseJSONParser(json_lines)
    base_json_parser.get_keys(base_json_parser.json_file)

    json_element_tree: JSONElementTree = JSONElementTree(base_json_parser)
    json_element_tree.set_element_tree()
    logger.info("Parsed JSON")
    logger.info("------------------------------")
    for element in base_json_parser.elements:
        logger.info(element)
        logger.info("------------------------------")
    return


@logger.catch
@configure_custom_logging(__name__)
def main():
    cmd_list: CommandList = CommandList(intercept_commands=_default_interceptors)
    _printhello = FunctionType(
        print_hello.__code__, print_hello.__globals__, print_hello.__name__, print_hello.__defaults__,
        print_hello.__closure__)
    _print_hello: Command = Command(_printhello, cmd_list)
    cmd_list + _print_hello
    # _split: Command = Command(split)
    # _recursive_iterdir: Command = Command(recursive_iterdir)
    # _stop: Command = Command(stop)
    # logger.info(f"{_print_hello.varnames}")
    user_input_parser: UserInputParser = UserInputParser(cmd_list)
    user_input_parser.get_input("command:\n")
    cmd: str = user_input_parser.prepare()
    cmd_list.get_command(cmd).__call__(*user_input_parser.pos_args, **user_input_parser.kw_args)
    print(f"print_hello.__code__.co_argcount = {print_hello.__code__.co_argcount}")
    print(f"print_hello.__code__.co_varnames = {print_hello.__code__.co_varnames}")
    print(f"print_hello.__code__.co_code = {print_hello.__code__.co_code}")
    print(f"print_hello.__code__.co_cellvars = {print_hello.__code__.co_cellvars}")
    print(f"print_hello.__code__.co_consts = {print_hello.__code__.co_consts}")
    print(f"print_hello.__code__.co_filename = {print_hello.__code__.co_filename}")
    print(f"print_hello.__code__.co_firstlineno = {print_hello.__code__.co_firstlineno}")
    print(f"print_hello.__code__.co_flags = {print_hello.__code__.co_flags}")
    print(f"print_hello.__code__.co_freevars = {print_hello.__code__.co_freevars}")
    print(f"print_hello.__code__.co_kwonlyargcount = {print_hello.__code__.co_kwonlyargcount}")
    print(f"print_hello.__code__.co_lines() = {print_hello.__code__.co_lines()}")
    print(f"print_hello.__code__.co_linetable = {print_hello.__code__.co_linetable}")
    print(f"print_hello.__code__.co_name = {print_hello.__code__.co_name}")
    print(f"print_hello.__code__.co_names = {print_hello.__code__.co_names}")
    print(f"print_hello.__code__.co_lnotab = {print_hello.__code__.co_lnotab}")
    print(f"print_hello.__code__.co_nlocals = {print_hello.__code__.co_nlocals}")
    print(f"print_hello.__code__.co_positions() = {print_hello.__code__.co_positions()}")
    print(f"print_hello.__code__.co_posonlyargcount = {print_hello.__code__.co_posonlyargcount}")


if __name__ == '__main__':
    main()
