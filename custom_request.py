import re
import socket
import urllib.parse
from functools import cached_property
from re import Pattern, Match
from socket import AF_INET, SOCK_STREAM
from typing import Any, Optional, Iterable


class ValidatorInvalidAttributeName(BaseException):
    """Incorrect attribute name in validator."""


class ValidatorInvalidAttributeValueType(BaseException):
    """Incorrect attribute value type in validator."""


class ValidatorAttributeValueLessMinimum(BaseException):
    """Attribute value is less than minimum in validator."""


class ValidatorAttributeValueGreaterMaximum(BaseException):
    """Attribute value is greater than maximum in validator."""


class ValidatorAttributeValueNotAllowed(BaseException):
    """Attribute value is not specified as allowed in validator."""


class ValidatorAttributeValuePermitted(BaseException):
    """Attribute value is specified as permitted in validator."""


class ValidatorAttributeValuePatternFailed(BaseException):
    """Attribute value does not meet pattern in validator."""


class CustomSocket:
    _ip_pattern: Pattern = re.compile(r"\d+\.\d+\.\d+.\d+")

    def __init__(self, host: str, port: int):
        self._socket = socket.socket(AF_INET, SOCK_STREAM)
        self._host: str = host
        self._port: int = port
        self._buffer: bytes = b""

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, value):
        self._host = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value

    @property
    def buffer(self):
        return self._buffer

    @buffer.setter
    def buffer(self, value):
        self._buffer = value

    def add_buffer(self, data: bytes):
        self.buffer = self.buffer.join(data)

    @property
    def address(self):
        return self.host, self.port

    @cached_property
    def dns_name(self):
        match: Match = re.match(self._ip_pattern, self.host)
        if match:
            return socket.gethostbyaddr(self.host)
        else:
            return self.host

    @cached_property
    def ip_address(self):
        match: Match = re.match(self._ip_pattern, self.host)
        if match:
            return self.host
        else:
            return socket.gethostbyname(self.host)

    def send(self, msg: str, buffer_size: int = 4096):
        with self._socket as s:
            s.bind(self.address)
            s.connect(self.address)
            s.sendall(bytes(msg))
            while True:
                data: bytes = s.recv(buffer_size)
                if not data:
                    break
                self.add_buffer(data)
        return self.buffer

    def decode(self):
        return self.buffer.decode("utf-8")


class AttributeValidator:
    _allowed_methods: tuple[str, ...] = ("GET", "POST", "PUT", "DELETE", "OPTIONS")
    _url_pattern: Pattern = re.compile(r"https?://.*\.\w+(/.*)?")

    def __init__(self, attrs: dict[str, Any] = None):
        if attrs is None:
            attrs = dict()
        self.attrs = attrs

    def __getattribute__(self, item):
        return self.attrs.__getattribute__(item)

    def __contains__(self, item):
        return item in self.attrs.keys()

    def validate_name(self, attr: str):
        if attr not in self:
            raise ValidatorInvalidAttributeName

    def validate_type(self, attr: str, type_: Optional[type] = None):
        if type_ is not None and not isinstance(self[attr], type_):
            raise ValidatorInvalidAttributeValueType

    def validate_min_range(self, attr: str, min_value: Optional[int] = None):
        if min_value is not None and self[attr] < min_value:
            raise ValidatorAttributeValueLessMinimum

    def validate_max_range(self, attr: str, max_value: Optional[int] = None):
        if max_value is not None and self[attr] > max_value:
            raise ValidatorAttributeValueGreaterMaximum

    def validate_whitelisted(self, attr: str, white_listed: Iterable = None):
        if white_listed is not None and self[attr] not in white_listed:
            raise ValidatorAttributeValueNotAllowed

    def validate_blacklisted(self, attr: str, black_listed: Iterable = None):
        if black_listed is not None and self[attr] in black_listed:
            raise ValidatorAttributeValuePermitted

    def validate_pattern(self, attr: str, pattern: Optional[Pattern] = None):
        if pattern is not None and re.match(pattern, self[attr]):
            raise ValidatorAttributeValuePatternFailed

    def validate(
            self,
            attr: str, *,
            type_: Optional[type] = None,
            ranges: tuple[Optional[int], Optional[int]] = (None, None),
            white_listed: Iterable = None,
            black_listed: Iterable = None,
            pattern: Optional[Pattern] = None):
        self.validate_name(attr)
        self.validate_type(attr, type_)
        min_value, max_value = ranges
        self.validate_min_range(attr, min_value)
        self.validate_max_range(attr, max_value)
        self.validate_whitelisted(attr, white_listed)
        self.validate_blacklisted(attr, black_listed)
        self.validate_pattern(attr, pattern)


class CustomHTTPRequest:
    _allowed_methods: tuple[str, ...] = ("GET", "POST", "PUT", "DELETE", "OPTIONS")
    _url_pattern: Pattern = re.compile(r"https?://.*\.\w+(/.*)?")

    def __init__(self,
                 method: str = None,
                 scheme: str = None,
                 netloc: str = None,
                 port: int = None,
                 web_hook: str = None,
                 headers: dict[str, str] = None,
                 params: dict[str, str] = None):
        if headers is None:
            headers = dict()
        if params is None:
            params = dict()

        self._method: str = method
        self._scheme = scheme
        self._netloc = netloc
        self._port = port
        self._headers: dict[str, str] = headers
        self._web_hook: str = web_hook
        self._params: dict[str, str] = params
        self._curl: list[str] = []

    @property
    def method(self):
        return self._method

    @method.setter
    def method(self, value):
        self._method = value

    @property
    def scheme(self):
        return self._scheme

    @scheme.setter
    def scheme(self, value):
        self._scheme = value

    @property
    def netloc(self):
        return self._netloc

    @netloc.setter
    def netloc(self, value):
        self._netloc = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value

    @property
    def headers(self):
        return self._headers

    @headers.setter
    def headers(self, value):
        self._headers = value

    @property
    def web_hook(self):
        return self._web_hook

    @web_hook.setter
    def web_hook(self, value):
        self._web_hook = value

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, value):
        self._params = value

    @property
    def curl(self):
        return self._curl

    @curl.setter
    def curl(self, value):
        self._curl = value

    def _attrs(self):
        return {
            "method": self.method,
            "url": self.url,
            "headers": self.headers,
            "web_hook": self.web_hook,
            "params": self.params
        }

    def _components(self):
        scheme = self.scheme
        netloc = f"{self.netloc}:{self.port}" if self.port else self.netloc
        path: str = self.web_hook
        query: str = "&".join(self._string_params())
        fragment = ""
        return scheme, netloc, path, urllib.parse.quote_plus(query, "/=&"), fragment

    def _prepare_scheme(self):
        self.scheme = self.scheme.strip(":/")

    def _prepare_netloc(self):
        self.netloc = self.netloc.strip("/")

    def _prepare_web_hook(self):
        match self.web_hook.startswith("/"), self.web_hook.endswith("/"):
            case True, True:
                self.web_hook = self.web_hook[:-1]
            case True, False:
                pass
            case False, True:
                self.web_hook = "".join(("/", self.web_hook))[:-1]
            case False, False:
                self.web_hook = "".join(("/", self.web_hook))
            case _, _:
                raise OSError

    def _string_params(self):
        return list(map(lambda x, y: f"{x}={y}", self.params.items()))

    def prepare_url(self):
        return urllib.parse.urlunparse(self._components())

    def _validate(self):
        validator: AttributeValidator = AttributeValidator(self._attrs())
        validator.validate("method", type_=str, white_listed=self._allowed_methods)
        validator.validate("url", type_=str, pattern=self._url_pattern)

    def _headers_string(self):
        headers: list[str] = [f"-H \'{header}: {value}\'" for header, value in self.headers.items()]


    def curlify(self):
        method: str = f"\'{self.method.upper()}\'"

        full_url: str = self.prepare_url()

        self.__add__(("curl -X", method, *headers, full_url))
        # self + "curl -X" + method + headers + full_url

        return " ".join(self.curl)

    def __add__(self, param):
        if isinstance(param, str):
            self.curl.append(param)
        elif isinstance(param, Iterable):
            for item in param:
                self.__add__(item)
        else:
            return NotImplemented