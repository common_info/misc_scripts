import glob
import hashlib
from _hashlib import HASH
from abc import abstractmethod
from enum import Enum, auto
from pathlib import Path
from typing import Any, Optional
from loguru import logger
from init_logger import configure_custom_logging


class ObjectType(Enum):
    FILE = auto()
    FOLDER = auto()
    NONE = auto()


class HashHandler:
    __doc__ = """
    Specifies the class to get hash of the file.

    Allowed algorithms: SHA2-256, SHA2-384, SHA2-512, MD5, SHA3-256, SHA3-384, SHA3-512.
    """
    _conversion_table: dict[str, Any] = {
        "SHA2-256": hashlib.sha256(),
        "SHA2-384": hashlib.sha384(),
        "SHA2-512": hashlib.sha512(),
        "MD5": hashlib.md5(),
        "SHA3-256": hashlib.sha3_256(),
        "SHA3-384": hashlib.sha3_384(),
        "SHA3-512": hashlib.sha3_512(),
    }
    _allowed_algorithms: tuple[str, ...] = tuple(_conversion_table)

    def __init__(self, *, algorithm: str = None, to_hash: str = None, object_type: ObjectType = ObjectType.NONE):
        self._algorithm: Optional[str] = algorithm
        self._to_hash: Optional[str] = to_hash
        self._object_type: Optional[ObjectType] = object_type

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._algorithm == other._algorithm
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._algorithm != other._algorithm
        else:
            return NotImplemented

    @property
    def algorithm(self):
        return self._algorithm

    @algorithm.setter
    def algorithm(self, value):
        self._algorithm = value

    @property
    def to_hash(self):
        return self._to_hash

    @to_hash.setter
    def to_hash(self, value):
        self._to_hash = value

    @property
    def object_type(self):
        return self._object_type.name

    @object_type.setter
    def object_type(self, value):
        if value in ObjectType.__members__.keys():
            self._object_type = value
        else:
            self._object_type = ObjectType.NONE

    def get_type(self):
        if self.to_hash is None:
            raise AttributeError
        path_to_hash: Path = Path(self.to_hash)
        if path_to_hash.is_dir():
            return ObjectType.FOLDER
        elif path_to_hash.is_file():
            return ObjectType.FILE
        else:
            return ObjectType.NONE

    def handler_factory(self, object_type: ObjectType):
        match object_type:
            case ObjectType.FILE:
                return HashFileHandler.make_handler(self.algorithm, self.to_hash)
            case ObjectType.FOLDER:
                return HashFolderHandler.make_handler(self.algorithm, self.to_hash)
            case _:
                return HashNullHandler.make_handler()

    @abstractmethod
    def get_hash(self, **kwargs):
        pass

    def hexdigest(self):
        return self.get_hash().hexdigest()


class HashNullHandler(HashHandler):
    @classmethod
    def make_handler(cls):
        return cls(algorithm=None, to_hash=None, object_type=ObjectType.NONE)

    def get_hash(self, **kwargs):
        return None

    def hexdigest(self):
        return None


class HashFileHandler(HashHandler):
    @classmethod
    def make_handler(cls, algorithm: str = None, to_hash: str = None):
        return cls(algorithm=algorithm, to_hash=to_hash, object_type=ObjectType.FILE)

    def get_hash(self, **kwargs) -> HASH:
        _hash: HASH = self._conversion_table[self.algorithm]
        try:
            with open(self.to_hash, "rb") as file_binary:
                for chunk in iter(lambda: file_binary.read(2 ** 20), b""):
                    _hash.update(chunk)
                    logger.debug(_hash.hexdigest())
        except PermissionError:
            raise
        except OSError:
            raise
        else:
            return _hash


class HashFolderHandler(HashHandler):
    @classmethod
    def make_handler(cls, algorithm: str = None, to_hash: str = None):
        return cls(algorithm=algorithm, to_hash=to_hash, object_type=ObjectType.FOLDER)

    def filenames(self):
        py_files = glob.glob(f"{self.to_hash}/*.py")
        in_files = glob.glob(f"{self.to_hash}/*.in")
        toml_files = glob.glob(f"{self.to_hash}/*.toml")
        txt_files = glob.glob(f"{self.to_hash}/*.txt")
        md_files = glob.glob(f"{self.to_hash}/*.md")
        return *py_files, *in_files, *toml_files, *txt_files, *md_files,

    def get_hash(self, **kwargs):
        _hash: HASH = self._conversion_table[self.algorithm]
        try:
            for filename in self.filenames():
                with open(filename, "rb") as file_binary:
                    for chunk in iter(lambda: file_binary.read(2 ** 20), b""):
                        _hash.update(chunk)
                        logger.debug(_hash.hexdigest())
        except PermissionError:
            raise
        except OSError:
            raise
        else:
            return _hash


@configure_custom_logging("hash")
def main():
    algorithm: str = input("Algorithm:\n")
    to_hash: str = input("Path to the object:\n")
    hash_handler: HashHandler = HashHandler(algorithm=algorithm, to_hash=to_hash)
    object_type: ObjectType = hash_handler.get_type()
    handler = hash_handler.handler_factory(object_type)
    logger.info(handler.get_hash().hexdigest())


if __name__ == "__main__":
    main()
