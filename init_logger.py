import logging
import time
from logging import Handler, LogRecord
from functools import cached_property
from pathlib import Path
from sys import stdout as sysout
from types import FrameType
from typing import Type, Literal, Optional, Any, Callable
from loguru import logger

__all__ = ["LoggerConfiguration", "HandlerType", "LoggingLevel", "custom_logging", "timer",
           "complete_configure_custom_logging", "configure_custom_logging"]

HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


def convert_ns_to_readable(time_ns: int) -> str:
    hours: int
    minutes: int
    seconds: int = time_ns // 10 ** 9
    if seconds == 0:
        return "менее секунды"
    if seconds < 60:
        hours = 0
        minutes = 0
    else:
        hours = 0
        minutes = seconds // 60
        seconds = seconds % 60
        if minutes >= 60:
            hours = minutes // 60
            minutes = minutes % 60
    hour_string: str = f"{hours} ч " if hours != 0 else ""
    minutes_string: str = f"{minutes} мин " if minutes != 0 else ""
    seconds_string: str = f"{seconds} с" if seconds != 0 else ""
    return f"{hour_string}{minutes_string}{seconds_string}"


def custom_logging(func: Callable):
    def wrapper(*args, **kwargs):
        args_string: str = "\n".join(arg for arg in args)
        kwargs_string: str = "\n".join(f"{k}={v}" for k, v in kwargs.items())
        logger.info(f"{func.__name__}({args_string}, {kwargs_string})")
        return func(*args, **kwargs)
    return wrapper


def timer(func: Callable):
    def wrapper(*args, **kwargs):
        start_time: int = time.perf_counter_ns()
        result = func(*args, **kwargs)
        end_time: int = time.perf_counter_ns()
        delta_time: int = end_time - start_time
        logger.debug(f"Время выполнения: {delta_time} наносекунд, {convert_ns_to_readable(delta_time)}\n")
        return result
    return wrapper


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


class LoggerConfiguration:
    _log_folder: Path = Path(__file__).parent.joinpath("logs")

    def __init__(
            self,
            file_name: str,
            handlers: dict[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers = dict()
        self._file_name: str = file_name
        self._handlers: dict[str, str] = handlers

    @staticmethod
    def _no_http(record: dict):
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self):
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self):
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self):
        return "{message}"

    def stream_handler(self):
        try:
            _logging_level: Optional[str] = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._user_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError:
            return
        else:
            return _handler

    def rotating_file_handler(self):
        try:
            _log_path: Path = self._log_folder.joinpath(f"{self._file_name}_debug.log")
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": _log_path,
                "level": _logging_level,
                "format": self._wb_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError:
            return
        else:
            return _handler


def configure_custom_logging(name: str):
    def inner(func: Callable):
        def wrapper(*args, **kwargs):
            handlers: dict[HandlerType, LoggingLevel] = {
                "stream": "INFO",
                "file_rotating": "DEBUG"
            }

            file_name: str = name

            logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
            stream_handler: dict[str, Any] = logger_configuration.stream_handler()
            rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

            logger.configure(handlers=[stream_handler, rotating_file_handler])

            logging.basicConfig(handlers=[InterceptHandler()], level=0)
            logger.debug("========================================")
            logger.debug("Запуск программы:")

            return func(*args, **kwargs)
        return wrapper
    return inner


def complete_configure_custom_logging(name: str):
    def inner(func: Callable):
        start_time: int = time.perf_counter_ns()

        handlers: dict[HandlerType, LoggingLevel] = {
            "stream": "INFO",
            "file_rotating": "DEBUG"
        }

        file_name: str = name

        logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
        stream_handler: dict[str, Any] = logger_configuration.stream_handler()
        rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

        logger.configure(handlers=[stream_handler, rotating_file_handler])

        logging.basicConfig(handlers=[InterceptHandler()], level=0)
        logger.debug("========================================")
        logger.debug("Запуск программы:")

        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        end_time: int = time.perf_counter_ns()
        delta_time: int = end_time - start_time
        logger.debug(f"Время выполнения: {delta_time} наносекунд, {convert_ns_to_readable(delta_time)}\n")

        return wrapper
    return inner
