from typing import Iterable


def join_values(values: Iterable[str]):
    return ", ".join(values)


def get_input():
    return iter(input, "")


def main():
    values: tuple[str, ...] = tuple(get_input())
    print(join_values(values))


if __name__ == "__main__":
    main()
