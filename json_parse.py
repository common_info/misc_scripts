import json
from json import JSONDecodeError
from typing import Optional, NamedTuple, Union, Iterable


class JSONFileElement(NamedTuple):
    key: str
    level: int
    parent: str
    element_type: str
    children: list[str] = None

    def __repr__(self):
        return f"<{self.__class__.__name__}(key={self.key}, level={self.level}, parent={self.parent}, " \
               f"element_type={self.element_type}, children={self.children})>"

    def __str__(self):
        return f"(level {self.level}){self.key}: <{self.element_type}> ({[child for child in self.children]})"

    @classmethod
    def get_element(
            cls,
            key: str,
            level: int = None,
            parent: str = None,
            element_type: str = None,
            children: list[str] = None):
        if level is None:
            level = 0
        if parent is None:
            parent = ""
        if element_type is None:
            element_type = ""
        if children is None:
            children = []
        return cls(key, level, parent, element_type, children)


class BaseJSONParser:
    _instance = None
    json_file: Union[dict, list, None] = None
    elements: list[JSONFileElement] = []

    def __new__(cls, input_json: str, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            if input_json.endswith("}"):
                try:
                    json_file: Optional[dict, list] = json.loads(input_json)
                except JSONDecodeError as e:
                    print(f"{e.__class__.__name__} Line {e.lineno} Col {e.colno} Pos {e.pos}\n{e.msg}")
                    raise
                except OSError as e:
                    print(f"{e.__class__.__name__}. Error {e.errno}\n{e.strerror}")
                    raise
                else:
                    cls.json_file = json_file
            else:
                try:
                    with open(input_json, "r") as file_json:
                        json_file: Optional[dict, list] = json.load(file_json)
                except RuntimeError as e:
                    print(f"{e.__class__.__name__}.\n{str(e)}")
                    raise
                except PermissionError as e:
                    print(f"{e.__class__.__name__}. Error {e.errno}\n{e.strerror}")
                    raise
                except OSError as e:
                    print(f"{e.__class__.__name__}. Error {e.errno}\n{e.strerror}")
                    raise
                else:
                    cls.json_file = json_file
        return cls._instance

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def get_keys(cls, json_file, level: int = 0, parent: str = None):
        if isinstance(json_file, dict):
            for k, v in json_file.items():
                element: JSONFileElement = JSONFileElement.get_element(k, level, parent, v.__class__.__name__, None)
                cls.elements.append(element)
                if isinstance(v, dict):
                    cls.get_keys(v, level + 1, k)
                elif isinstance(v, list):
                    for item in v:
                        cls.get_keys(item, level + 1, k)
        elif isinstance(json_file, list):
            for item in json_file:
                cls.get_keys(item, level, parent)

    @classmethod
    def _find_element_key(cls, key: str):
        for element in cls.elements:
            if element.key == key:
                return element

    @classmethod
    def __getitem__(cls, item):
        if not isinstance(item, str):
            return NotImplemented
        else:
            if item not in cls.element_keys():
                raise KeyError
            return cls._find_element_key(item)

    @classmethod
    def element_keys(cls):
        return list(map(lambda x: x.key, cls.elements))

    # @classmethod
    # def _exclude_root(cls):
    #     return list(filter(lambda x: x.level != 0, cls.elements))

    @classmethod
    def _set_type(cls, element_type: str):
        _cls_conversion: dict[str, type] = {
            "str": str,
            "int": int,
            "dict": dict,
            "list": list,
            "tuple": list,
            "object": dict,
            "bool": bool,
        }
        return _cls_conversion[element_type]


class JSONElementTree:
    def __init__(self, base_json_parser: BaseJSONParser):
        self._base_json_parser: BaseJSONParser = base_json_parser
        self._elements: Optional[list[JSONFileElement]] = []
        print(self.elements)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item not in self._element_keys():
                raise KeyError
            return self._find_element_key(item)
        elif isinstance(item, int):
            return self.elements.__getitem__(item)
        else:
            return NotImplemented

    def _element_keys(self):
        print(self._base_json_parser.element_keys())
        return self._base_json_parser.element_keys()

    def _find_element_key(self, key: str):
        for element in self.elements:
            if element.key == key:
                return key

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, value: Optional[Iterable[JSONFileElement]]):
        if value is None:
            self._elements = value
        else:
            self._elements = [*value]

    def _get_children(self, element_key: str):
        if element_key not in self._element_keys():
            raise KeyError
        return tuple(filter(lambda x: x.parent == element_key, self._base_json_parser.elements))

    def _get_root(self):
        for element in self._base_json_parser.elements:
            if element.level == 0:
                print(element)
                return element

    def __delitem__(self, key):
        if isinstance(key, int):
            del self[key]
            return
        elif isinstance(key, str):
            del self[key]
            return
        elif isinstance(key, Iterable):
            for item in key:
                del self[item]
            return
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, JSONFileElement):
            self.elements.append(other)
            return
        elif isinstance(other, Iterable) and all(isinstance(item, JSONFileElement) for item in other):
            self.elements.extend(other)
            return
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, JSONFileElement):
            try:
                self._elements.remove(other)
            except ValueError:
                pass
            return
        elif isinstance(other, Iterable) and all(isinstance(item, JSONFileElement) for item in other):
            for item in other:
                try:
                    self._elements.remove(item)
                except ValueError:
                    continue
                return
        else:
            return NotImplemented

    def _set_children(self, element: JSONFileElement):
        children: tuple[JSONFileElement, ...] = self._get_children(element.key)
        for child in children:
            element.children.append(child.key)
            self + child
            self._set_children(child)

    def set_element_tree(self):
        root: JSONFileElement = self._get_root()
        print(root)
        self + root
        self._set_children(root)

    def __str__(self):
        return "\n".join(str(element) for element in self.elements)


def main():
    sentinel = ""
    json_lines = "\n".join(iter(input, sentinel))

    base_json_parser: BaseJSONParser = BaseJSONParser(json_lines)
    base_json_parser.get_keys(base_json_parser.json_file)

    json_element_tree: JSONElementTree = JSONElementTree(base_json_parser)
    print(json_element_tree)
    json_element_tree.set_element_tree()
    print(str(json_element_tree))

    print("------------------------------")
    for element in base_json_parser.elements:
        print(element)
        print("------------------------------")


if __name__ == "__main__":
    main()
