import ctypes
from ctypes import wintypes, Structure, c_void_p, c_int, Union, windll, CFUNCTYPE, POINTER, c_uint, byref, pointer
from os import environ
from platform import machine
from typing import Optional
import six
import atexit
import sys
import time

from loguru import logger

DEBUG = 0

win32_keys: dict[str, int] = {
    "VK_BACK": 8,  # BACKSPACE key
    "VK_TAB": 9,  # TAB key
    "VK_CLEAR": 12,  # CLEAR key
    "VK_RETURN": 13,  # ENTER key
    "VK_SHIFT": 16,  # SHIFT key
    "VK_CONTROL": 17,  # CTRL key
    "VK_MENU": 18,  # ALT key
    "VK_PAUSE": 19,  # PAUSE key
    "VK_CAPITAL": 20,  # CAPS LOCK key
    "VK_KANA": 21,  # IME Kana mode
    "VK_HANGUEL": 21,  # IME Hanguel mode (maintained for compatibility; use VK_HANGUL)
    "VK_HANGUL": 21,  # IME Hangul mode
    "VK_IME_ON": 22,  # IME On
    "VK_JUNJA": 23,  # IME Junja mode
    "VK_FINAL": 24,  # IME final mode
    "VK_HANJA": 25,  # IME Hanja mode
    "VK_KANJI": 25,  # IME Kanji mode
    "VK_IME_OFF": 26,  # IME Off
    "VK_ESCAPE": 27,  # ESC key
    "VK_CONVERT": 28,  # IME convert
    "VK_NONCONVERT": 29,  # IME nonconvert
    "VK_ACCEPT": 30,  # IME accept
    "VK_MODECHANGE": 31,  # IME mode change request
    "VK_SPACE": 32,  # SPACEBAR
    "VK_PRIOR": 33,  # PAGE UP key
    "VK_NEXT": 34,  # PAGE DOWN key
    "VK_END": 35,  # END key
    "VK_HOME": 36,  # HOME key
    "VK_LEFT": 37,  # LEFT ARROW key
    "VK_UP": 38,  # UP ARROW key
    "VK_RIGHT": 39,  # RIGHT ARROW key
    "VK_DOWN": 40,  # DOWN ARROW key
    "VK_SELECT": 41,  # SELECT key
    "VK_PRINT": 42,  # PRINT key
    "VK_EXECUTE": 43,  # EXECUTE key
    "VK_SNAPSHOT": 44,  # PRINT SCREEN key
    "VK_INSERT": 45,  # INS key
    "VK_DELETE": 46,  # DEL key
    "VK_HELP": 47,  # HELP key
    "VK_0": 48,  # 0 key
    "VK_1": 49,  # 1 key
    "VK_2": 50,  # 2 key
    "VK_3": 51,  # 3 key
    "VK_4": 52,  # 4 key
    "VK_5": 53,  # 5 key
    "VK_6": 54,  # 6 key
    "VK_7": 55,  # 7 key
    "VK_8": 56,  # 8 key
    "VK_9": 57,  # 9 key
    "VK_A": 65,  # A key
    "VK_B": 66,  # B key
    "VK_C": 67,  # C key
    "VK_D": 68,  # D key
    "VK_E": 69,  # E key
    "VK_F": 70,  # F key
    "VK_G": 71,  # G key
    "VK_H": 72,  # H key
    "VK_I": 73,  # I key
    "VK_J": 74,  # J key
    "VK_K": 75,  # K key
    "VK_L": 76,  # L key
    "VK_M": 77,  # M key
    "VK_N": 78,  # N key
    "VK_O": 79,  # O key
    "VK_P": 80,  # P key
    "VK_Q": 81,  # Q key
    "VK_R": 82,  # R key
    "VK_S": 83,  # S key
    "VK_T": 84,  # T key
    "VK_U": 85,  # U key
    "VK_V": 86,  # V key
    "VK_W": 87,  # W key
    "VK_X": 88,  # X key
    "VK_Y": 89,  # Y key
    "VK_Z": 90,  # Z key
    "VK_LWIN": 91,  # Left Windows key (Natural keyboard)
    "VK_RWIN": 92,  # Right Windows key (Natural keyboard)
    "VK_APPS": 93,  # Applications key (Natural keyboard)
    "VK_SLEEP": 95,  # Computer Sleep key
    "VK_NUMPAD0": 96,  # Numeric keypad 0 key
    "VK_NUMPAD1": 97,  # Numeric keypad 1 key
    "VK_NUMPAD2": 98,  # Numeric keypad 2 key
    "VK_NUMPAD3": 99,  # Numeric keypad 3 key
    "VK_NUMPAD4": 100,  # Numeric keypad 4 key
    "VK_NUMPAD5": 101,  # Numeric keypad 5 key
    "VK_NUMPAD6": 102,  # Numeric keypad 6 key
    "VK_NUMPAD7": 103,  # Numeric keypad 7 key
    "VK_NUMPAD8": 104,  # Numeric keypad 8 key
    "VK_NUMPAD9": 105,  # Numeric keypad 9 key
    "VK_MULTIPLY": 106,  # Multiply key
    "VK_ADD": 107,  # Add key
    "VK_SEPARATOR": 108,  # Separator key
    "VK_SUBTRACT": 109,  # Subtract key
    "VK_DECIMAL": 110,  # Decimal key
    "VK_DIVIDE": 111,  # Divide key
    "VK_F1": 112,  # F1 key
    "VK_F2": 113,  # F2 key
    "VK_F3": 114,  # F3 key
    "VK_F4": 115,  # F4 key
    "VK_F5": 116,  # F5 key
    "VK_F6": 117,  # F6 key
    "VK_F7": 118,  # F7 key
    "VK_F8": 119,  # F8 key
    "VK_F9": 120,  # F9 key
    "VK_F10": 121,  # F10 key
    "VK_F11": 122,  # F11 key
    "VK_F12": 123,  # F12 key
    "VK_F13": 124,  # F13 key
    "VK_F14": 125,  # F14 key
    "VK_F15": 126,  # F15 key
    "VK_F16": 127,  # F16 key
    "VK_F17": 128,  # F17 key
    "VK_F18": 129,  # F18 key
    "VK_F19": 130,  # F19 key
    "VK_F20": 131,  # F20 key
    "VK_F21": 132,  # F21 key
    "VK_F22": 133,  # F22 key
    "VK_F23": 134,  # F23 key
    "VK_F24": 135,  # F24 key
    "VK_NUMLOCK": 144,  # NUM LOCK key
    "VK_SCROLL": 145,  # SCROLL LOCK key
    "VK_LSHIFT": 160,  # Left SHIFT key
    "VK_RSHIFT": 161,  # Right SHIFT key
    "VK_LCONTROL": 162,  # Left CONTROL key
    "VK_RCONTROL": 163,  # Right CONTROL key
    "VK_LMENU": 164,  # Left ALT key
    "VK_RMENU": 165,  # Right ALT key
    "VK_BROWSER_BACK": 166,  # Browser Back key
    "VK_BROWSER_FORWARD": 167,  # Browser Forward key
    "VK_BROWSER_REFRESH": 168,  # Browser Refresh key
    "VK_BROWSER_STOP": 169,  # Browser Stop key
    "VK_BROWSER_SEARCH": 170,  # Browser Search key
    "VK_BROWSER_FAVORITES": 171,  # Browser Favorites key
    "VK_BROWSER_HOME": 172,  # Browser Start and Home key
    "VK_VOLUME_MUTE": 173,  # Volume Mute key
    "VK_VOLUME_DOWN": 174,  # Volume Down key
    "VK_VOLUME_UP": 175,  # Volume Up key
    "VK_MEDIA_NEXT_TRACK": 176,  # Next Track key
    "VK_MEDIA_PREV_TRACK": 177,  # Previous Track key
    "VK_MEDIA_STOP": 178,  # Stop Media key
    "VK_MEDIA_PLAY_PAUSE": 179,  # Play/Pause Media key
    "VK_LAUNCH_MAIL": 180,  # Start Mail key
    "VK_LAUNCH_MEDIA_SELECT": 181,  # Select Media key
    "VK_LAUNCH_APP1": 182,  # Start Application 1 key
    "VK_LAUNCH_APP2": 183,  # Start Application 2 key
    "VK_OEM_1": 186,
    # Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the ';:' key
    "VK_OEM_PLUS": 187,  # For any country/region, the '+' key
    "VK_OEM_COMMA": 188,  # For any country/region, the ',' key
    "VK_OEM_PERIOD": 190,  # For any country/region, the '.' key
    "VK_OEM_MINUS": 189,  # For any country/region, the '-' key
    "VK_OEM_2": 191,
    # Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '/?' key
    "VK_OEM_3": 192,
    # Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '`~' key
    "VK_OEM_4": 219,
    # Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '[{' key
    "VK_OEM_5": 220,
    # Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '\|' key
    "VK_OEM_6": 221,
    # Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the ']}' key
    "VK_OEM_7": 222,
    # Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard,
    # the 'single-quote/double-quote' key
    "VK_OEM_8": 223,  # Used for miscellaneous characters; it can vary by keyboard.
    "VK_OEM_102": 226,  # The <> keys on the US standard keyboard, or the \| key on the non-US 102-key keyboard
    "VK_PROCESSKEY": 229,  # IME PROCESS key
    "VK_PACKET": 231,
    # Used to pass Unicode characters as if they were keystrokes. The VK_PACKET key is the low word of a 32-bit
    # Virtual Key value used for non-keyboard input methods. For more information, see Remark in KEYBDINPUT,
    # SendInput, WM_KEYDOWN, and WM_KEYUP
    "VK_ATTN": 246,  # Attn key
    "VK_CRSEL": 247,  # CrSel key
    "VK_EXSEL": 248,  # ExSel key
    "VK_EREOF": 249,  # Erase EOF key
    "VK_PLAY": 250,  # Play key
    "VK_ZOOM": 251,  # Zoom key
    "VK_NONAME": 252,  # Reserved
    "VK_PA1": 253,  # PA1 key
    "VK_OEM_CLEAR": 254  # Clear key
}

win32_mouse: dict[str, int] = {
    "VK_LBUTTON": 1,  # Left mouse button
    "VK_RBUTTON": 2,  # Right mouse button
    "VK_CANCEL": 3,  # Control-break processing
    "VK_MBUTTON": 4,  # Middle mouse button (three-button mouse)
    "VK_XBUTTON1": 5,  # X1 mouse button
    "VK_XBUTTON2": 6  # X2 mouse button
}

win32_flags: dict[str, int] = {
    "INPUT_KEYBOARD": 1,
    "KEYEVENTF_EXTENDEDKEY": 1,
    "KEYEVENTF_KEYUP": 2,
    "KEYEVENTF_UNICODE": 4,
    "KEYEVENTF_SCANCODE": 8,
    "VK_SHIFT": 16,
    "VK_CONTROL": 17,
    "VK_MENU": 18
}

vk_ascii: dict[str, int] = {
    ' ': 32,
    '=': 187,
    ',': 188,
    '-': 189,
    '.': 190,
    ';': 186,
    '/': 191,
    '`': 192,
    '[': 219,
    '\\': 220,
    ']': 221,
    '\'': 222
}

action_buttons: dict[str, int] = {
    '+': win32_keys.get("VK_SHIFT"),
    '^': win32_keys.get("VK_CONTROL"),
    '%': win32_keys.get("VK_MENU")
}

win32_names: dict[int, str] = {v: k for k, v in win32_keys.items()}

win32_events: dict[str, int] = {
    "WM_KEYDOWN": 256,  # WM_KEYDOWN for normal keys
    "WM_KEYUP": 257,  # WM_KEYUP for normal keys
    "WM_SYSKEYDOWN": 260,  # WM_SYSKEYDOWN, is used for Alt key
    "WM_SYSKEYUP": 261,  # WM_SYSKEYUP, is used for Alt key
    "WM_QUIT": 18  # WM_QUIT, stop
}

short = wintypes.SHORT
long = wintypes.LONG
word = wintypes.WORD
dword = wintypes.DWORD
dword_ptr = uint_ptr = ulong_ptr = ctypes.c_size_t
wchar = wintypes.WCHAR
hkl = wintypes.HKL
process_query_information = 1024
true = 1
uint = wintypes.UINT
point = wintypes.POINT
l_result = wintypes.LPARAM

SendInput = ctypes.windll.user32.SendInput
SendInput.restype = uint
SendInput.argtypes = [uint, c_void_p, c_int]

GetMessageExtraInfo = ctypes.windll.user32.GetMessageExtraInfo

MapVirtualKeyW = ctypes.windll.user32.MapVirtualKeyW

VkKeyScanW = ctypes.windll.user32.VkKeyScanW
VkKeyScanW.restype = short
VkKeyScanW.argtypes = [wchar]

wh_keyboard_ll = 13
wh_mouse_ll = 14
pm_remove = 1
wm_quit = 18


class SystemInfo:
    _instance = None
    _os_architecture: Optional[str] = None
    _x64_os: tuple[str, ...] = ("x86_64", "ia64")
    _architecture_dict: dict[str, str] = {
        'x86': 'x86',
        'i386': 'x86',
        'i486': 'x86',
        'i586': 'x86',
        'i686': 'x86',
        'x64': 'x86_64',
        'AMD64': 'x86_64',
        'amd64': 'x86_64',
        'em64t': 'x86_64',
        'EM64T': 'x86_64',
        'x86_64': 'x86_64',
        'IA64': 'ia64',
        'ia64': 'ia64'
    }

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            if sys.platform == 'win32':
                arch: str = environ.get('PROCESSOR_ARCHITEW6432', '')
                if arch == '':
                    arch: str = environ.get('PROCESSOR_ARCHITECTURE', '')
                cls._os_architecture: Optional[str] = cls._architecture_dict.get(arch, None)
            else:
                cls._os_architecture: Optional[str] = cls._architecture_dict.get(machine(), None)
        return cls._instance

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def bits(cls) -> int:
        return ctypes.sizeof(ctypes.POINTER(ctypes.c_int)) * 8

    @classmethod
    def is_x64_python(cls) -> bool:
        return cls.bits() == 64

    @classmethod
    def is_x64_os(cls) -> bool:
        return cls._os_architecture in cls._x64_os


class StructureMixIn(object):
    """Define printing and comparison behaviors to be used for the Structure class from ctypes"""

    def __str__(self):
        """Print out the fields of the ctypes Structure
        fields in exceptList will not be printed"""
        lines = []
        for field_name, _ in getattr(self, "_fields_", []):
            lines.append("%20s\t%s" % (field_name, getattr(self, field_name)))

        return "\n".join(lines)

    def __eq__(self, other):
        """Return True if the two instances have the same coordinates"""
        fields = getattr(self, "_fields_", [])
        if isinstance(other, Structure):
            try:
                # pretend they are two structures - check that they both
                # have the same value for all fields
                if len(fields) != len(getattr(other, "_fields_", [])):
                    return False
                for field_name, _ in fields:
                    if getattr(self, field_name) != getattr(other, field_name):
                        return False
                return True

            except AttributeError:
                return False

        elif isinstance(other, (list, tuple)):
            # Now try to see if we have been passed in a list or tuple
            if len(fields) != len(other):
                return False
            try:
                for i, (field_name, _) in enumerate(fields):
                    if getattr(self, field_name) != other[i]:
                        return False
                return True
            except Exception:
                return False
        return False

    def __ne__(self, other):
        """Return False if the two instances have the same coordinates"""
        return not self.__eq__(other)

    __hash__ = None


class MouseInput(Structure):
    if SystemInfo().is_x64_python():
        _pack_ = 8
    else:
        _pack_ = 2

    _fields_ = [
        ("dx", long),
        ("dy", long),
        ("mouse_data", dword),
        ("dw_flags", dword),
        ("time", dword),
        ("dw_extra_info", ulong_ptr)
    ]


class KeybdInput(Structure):
    if SystemInfo().is_x64_python():
        _pack_ = 8
    else:
        _pack_ = 2

    _fields_ = [
        ('w_vk', word),
        ('w_scan', word),
        ('dw_flags', dword),
        ('time', dword),
        ('dw_extra_info', ulong_ptr)
    ]


class HardwareInput(Structure):
    if SystemInfo().is_x64_python():
        _pack_ = 8
    else:
        _pack_ = 2

    _fields_ = [
        ('u_msg', dword),
        ('w_param_l', word),
        ('w_param_h', word),
    ]


class UnionInputStructs(Union):
    _fields_ = [
        # C:/PROGRA~1/MICROS~4/VC98/Include/winuser.h 4314
        ('mi', MouseInput),
        ('ki', KeybdInput),
        ('hi', HardwareInput)
    ]


class Input(Structure):
    if SystemInfo().is_x64_python():
        _pack_ = 8
    else:
        _pack_ = 2

    _anonymous_ = ("_",)
    _fields_ = [
        ('type', c_int),
        # Unnamed field renamed to '_'
        ('_', UnionInputStructs),
    ]


class Point(point, StructureMixIn):

    """Wrap the Point structure and add extra functionality"""

    def __iter__(self):
        """Allow iteration through coordinates"""
        yield self.x
        yield self.y

    def __getitem__(self, key):
        """Allow indexing of coordinates"""
        if key == 0 or key == -2:
            return self.x
        elif key == 1 or key == -1:
            return self.y
        else:
            raise IndexError("Illegal index")


class KbDllHookStruct(Structure):
    """Wrap KbDllHookStruct structure"""
    _fields_ = [
        ('vk_code', dword),
        ('scan_code', dword),
        ('flags', dword),
        ('time', dword),
        ('dw_extra_info', dword),
    ]


class MsLLHookStruct(Structure):
    """Wrap MsLLHookStruct structure"""
    _fields_ = [
        ('pt', Point),
        ('mouseData', dword),
        ('flags', dword),
        ('time', dword),
        ('dwExtraInfo', dword),
    ]


class KeySequenceError(Exception):
    """Exception raised when a key sequence string has a syntax error"""

    def __str__(self):
        return ' '.join(self.args)


class KeyAction(object):

    """Class that represents a single keyboard action
    It represents either a PAUSE action (not really keyboard) or a keyboard
    action (press or release or both) of a particular key.
    """

    def __init__(self, key, down=True, up=True):
        self.key = key
        if isinstance(self.key, six.string_types):
            self.key = six.text_type(key)
        self.down = down
        self.up = up

    def _get_key_info(self):
        """Return virtual_key, scan_code, and flags for the action
        This is one of the methods that will be overridden by subclasses.
        """
        return 0, ord(self.key), win32_flags.get("KEYEVENTF_UNICODE")

    def get_key_info(self):
        """Return virtual_key, scan_code, and flags for the action
        This is one of the methods that will be overridden by subclasses.
        """
        return self._get_key_info()

    def get_input(self):
        """Build the INPUT structure for the action"""
        actions = 1
        # if both up and down
        if self.up and self.down:
            actions = 2

        inputs = (Input * actions)()

        vk, scan, flags = self._get_key_info()

        for inp in inputs:
            inp.type = win32_flags.get("INPUT_KEYBOARD")

            inp.ki.wVk = vk
            inp.ki.wScan = scan
            inp.ki.dwFlags |= flags

            # it seems to return 0 every time, but it's required by MSDN specification
            # so call it just in case
            inp.ki.dwExtraInfo = GetMessageExtraInfo()

        # if we are releasing - then let it up
        if self.up:
            inputs[-1].ki.dwFlags |= win32_flags.get("KEYEVENTF_KEYUP")

        return inputs

    def run(self):
        """Execute the action"""
        inputs = self.get_input()

        # SendInput() supports all Unicode symbols
        num_inserted_events = SendInput(len(inputs), ctypes.byref(inputs),
                                        ctypes.sizeof(Input))
        if num_inserted_events != len(inputs):
            raise RuntimeError('SendInput() inserted only ' + str(num_inserted_events) +
                               ' out of ' + str(len(inputs)) + ' keyboard events')

    def _get_down_up_string(self):
        """Return a string that will show whether the string is up or down
        return 'down' if the key is a press only
        return 'up' if the key is up only
        return '' if the key is up & down (as default)
        """
        down_up = ""
        if not (self.down and self.up):
            if self.down:
                down_up = "down"
            elif self.up:
                down_up = "up"
        return down_up

    def key_description(self):
        """Return a description of the key"""
        vk, scan, flags = self._get_key_info()
        if vk:
            if vk in win32_names:
                desc = win32_names.get(vk)
            else:
                desc = f"VK {vk}"
        else:
            desc = f"{self.key}"
        return desc

    def __str__(self):
        parts = [self.key_description()]
        up_down = self._get_down_up_string()
        if up_down:
            parts.append(up_down)

        return f"<{' '.join(parts)}>"

    __repr__ = __str__


class VirtualKeyAction(KeyAction):

    """Represents a virtual key action e.g. F9 DOWN, etc.
    Overrides necessary methods of KeyAction
    """

    def _get_key_info(self):
        """Virtual keys have extended flag set"""
        # copied more or less verbatim from
        # http://www.pinvoke.net/default.aspx/user32.sendinput
        if 33 <= win32_keys.get(self.key) <= 46 or 91 <= win32_keys.get(self.key) <= 93:
            flags = win32_flags.get("KEYEVENTF_EXTENDEDKEY")
        else:
            flags = 0
        # This works for %{F4} - ALT + F4
        # return self.key, 0, 0

        # this works for Tic Tac Toe i.e. +{RIGHT} SHIFT + RIGHT
        return self.key, MapVirtualKeyW(self.key, 0), flags

    def run(self):
        """Execute the action"""
        # it works more stable for virtual keys than SendInput
        for inp in self.get_input():
            SendInput(inp.ki.wVk, inp.ki.wScan, inp.ki.dwFlags)


class EscapedKeyAction(KeyAction):

    """Represents an escaped key action e.g. F9 DOWN, etc.
    Overrides necessary methods of KeyAction
    """

    def _get_key_info(self):
        """EscapedKeyAction doesn't send it as Unicode
        The vk and scan code are generated differently.
        """
        vkey_scan = lo_byte(VkKeyScanW(self.key))

        return vkey_scan, MapVirtualKeyW(vkey_scan, 0), 0

    def key_description(self):
        """Return a description of the key"""
        return "KEsc {}".format(self.key)

    def run(self):
        """Execute the action"""
        # it works more stable for virtual keys than SendInput
        for inp in self.get_input():
            SendInput(inp.ki.wVk, inp.ki.wScan, inp.ki.dwFlags)


class PauseAction(KeyAction):

    """Represents a pause action"""

    def __init__(self, how_long, key=None):
        super().__init__(key)
        self.how_long = how_long

    def run(self):
        """Pause for the lenght of time specified"""
        time.sleep(self.how_long)

    def __str__(self):
        return "<PAUSE %1.2f>" % self.how_long

    __repr__ = __str__


def handle_code(code, vk_packet):
    """Handle a key or sequence of keys in braces"""
    code_keys = []
    # it is a known code (e.g. {DOWN}, {ENTER}, etc)
    if code in win32_keys:
        code_keys.append(VirtualKeyAction(win32_keys.get(code)))

    # it is an escaped modifier e.g. {%}, {^}, {+}
    elif len(code) == 1:
        if not vk_packet and code in vk_ascii:
            code_keys.append(VirtualKeyAction(vk_ascii.get(code)))
        else:
            code_keys.append(KeyAction(code))

    # it is a repetition or a pause  {DOWN 5}, {PAUSE 1.3}
    elif ' ' in code:
        to_repeat, count = code.rsplit(None, 1)
        if to_repeat == "PAUSE":
            try:
                pause_time = float(count)
            except ValueError:
                raise KeySequenceError(f'invalid pause time {count}')
            code_keys.append(PauseAction(pause_time))

        else:
            try:
                count = int(count)
            except ValueError:
                raise KeySequenceError(
                    'invalid repetition count {}'.format(count))

            # If the value in to_repeat is a VK e.g. DOWN
            # we need to add the code repeated
            if to_repeat in win32_keys:
                code_keys.extend(
                    [VirtualKeyAction(win32_keys.get(to_repeat))] * count)
            # otherwise parse the keys and we get back a KeyAction
            else:
                to_repeat = parse_keys(to_repeat, vk_packet=vk_packet)
                if isinstance(to_repeat, list):
                    keys = to_repeat * count
                else:
                    keys = [to_repeat] * count
                code_keys.extend(keys)
    else:
        raise RuntimeError("Unknown code: {}".format(code))

    return code_keys


def parse_keys(
        string,
        with_spaces=False,
        with_tabs=False,
        with_newlines=False,
        modifiers=None,
        vk_packet=True):
    """Return the parsed keys"""
    keys = []
    if not modifiers:
        modifiers = []

    should_escape_next_keys = False
    index = 0
    while index < len(string):

        c = string[index]
        index += 1
        # check if one of CTRL, SHIFT, ALT has been pressed
        if c in action_buttons.keys():
            modifier = action_buttons.get(c)
            # remember that we are currently modified
            modifiers.append(modifier)
            # hold down the modifier key
            keys.append(VirtualKeyAction(modifier, up=False))
            if DEBUG:
                print("MODS+", modifiers)
            continue

        # Apply modifiers over a bunch of characters (not just one!)
        elif c == "(":
            # find the end of the bracketed text
            end_pos = string.find(")", index)
            if end_pos == -1:
                raise KeySequenceError('`)` not found')
            keys.extend(parse_keys(
                string[index:end_pos],
                modifiers=modifiers,
                vk_packet=vk_packet))
            index = end_pos + 1

        # Escape or named key
        elif c == "{":
            # We start searching from index + 1 to account for the case {}}
            end_pos = string.find("}", index + 1)
            if end_pos == -1:
                raise KeySequenceError('`}` not found')

            code = string[index:end_pos]
            index = end_pos + 1
            key_events = [' up', ' down']
            current_key_event = None
            if any(key_event in code.lower() for key_event in key_events):
                code, current_key_event = code.split(' ')
                should_escape_next_keys = True
            current_keys = handle_code(code, vk_packet)
            if current_key_event is not None:
                if isinstance(current_keys[0].key, six.string_types):
                    current_keys[0] = EscapedKeyAction(current_keys[0].key)

                if current_key_event.strip() == 'up':
                    current_keys[0].down = False
                else:
                    current_keys[0].up = False
            keys.extend(current_keys)

        # unmatched ")"
        elif c == ')':
            raise KeySequenceError('`)` should be preceeded by `(`')

        # unmatched "}"
        elif c == '}':
            raise KeySequenceError('`}` should be preceeded by `{`')

        # so it is a normal character
        else:
            # don't output white space unless flags to output have been set
            if (c == ' ' and not with_spaces
                    or c == '\t' and not with_tabs
                    or c == '\n' and not with_newlines):
                continue

            # output newline
            if c in ('~', '\n'):
                keys.append(VirtualKeyAction(win32_keys.get("ENTER")))

            # safest are the virtual keys - so if our key is a virtual key
            # use a VirtualKeyAction
            # if ord(c) in CODE_NAMES:
            #    keys.append(VirtualKeyAction(ord(c)))

            elif modifiers or should_escape_next_keys:
                keys.append(EscapedKeyAction(c))

            # if user disables the vk_packet option, always try to send a
            # virtual key of the actual keystroke
            elif not vk_packet and c in vk_ascii:
                keys.append(VirtualKeyAction(vk_ascii.get(c)))

            else:
                keys.append(KeyAction(c))

        # as we have handled the text - release the modifiers
        while modifiers:
            if DEBUG:
                print("MODS-", modifiers)
            keys.append(VirtualKeyAction(modifiers.pop(), down=False))

    # just in case there were any modifiers left pressed - release them
    while modifiers:
        keys.append(VirtualKeyAction(modifiers.pop(), down=False))

    return keys


def lo_byte(value):
    """Return the low byte of the value"""
    return value & 0xff


def hi_byte(value):
    """Return the high byte of the value"""
    return (value & 0xff00) >> 8


def send_keys(
        keys,
        pause=0.05,
        with_spaces=False,
        with_tabs=False,
        with_newlines=False,
        turn_off_numlock=True,
        vk_packet=True):
    """Parse the keys and type them"""
    keys = parse_keys(
        keys, with_spaces, with_tabs, with_newlines, vk_packet=vk_packet)

    for k in keys:
        k.run()
        time.sleep(pause)


hook_cb = CFUNCTYPE(l_result, c_int, wintypes.WPARAM, wintypes.LPARAM)

windll.kernel32.GetModuleHandleA.restype = wintypes.HMODULE
windll.kernel32.GetModuleHandleA.argtypes = [wintypes.LPCSTR]
windll.user32.SetWindowsHookExA.restype = wintypes.HHOOK
windll.user32.SetWindowsHookExA.argtypes = [c_int, hook_cb, wintypes.HINSTANCE, wintypes.DWORD]
windll.user32.SetWindowsHookExW.restype = wintypes.HHOOK
windll.user32.SetWindowsHookExW.argtypes = [c_int, hook_cb, wintypes.HINSTANCE, wintypes.DWORD]
windll.user32.TranslateMessage.argtypes = [POINTER(wintypes.MSG)]
windll.user32.DispatchMessageW.argtypes = [POINTER(wintypes.MSG)]

windll.user32.PeekMessageW.argtypes = [POINTER(wintypes.MSG), wintypes.HWND, c_uint, c_uint, c_uint]
windll.user32.PeekMessageW.restypes = wintypes.BOOL

windll.user32.CallNextHookEx.argtypes = [wintypes.HHOOK, c_int, wintypes.WPARAM, wintypes.LPARAM]
windll.user32.CallNextHookEx.restypes = l_result


class KeyboardEvent(object):

    """Created when a keyboard event happened"""

    def __init__(self, current_key=None, event_type=None, pressed_key=None):
        self.current_key = current_key
        self.event_type = event_type
        self.pressed_key = pressed_key


class MouseEvent(object):

    """Created when a mouse event happened"""

    def __init__(self, current_key=None, event_type=None, mouse_x=0, mouse_y=0):
        self.current_key = current_key
        self.event_type = event_type
        self.mouse_x = mouse_x
        self.mouse_y = mouse_y


class Hook(object):

    """Hook for low level keyboard and mouse events"""

    MOUSE_ID_TO_KEY = {"WM_MOUSEMOVE": 'Move',
                       "WM_LBUTTONDOWN": 'LButton',
                       "WM_LBUTTONUP": 'LButton',
                       "WM_RBUTTONDOWN": 'RButton',
                       "WM_RBUTTONUP": 'RButton',
                       "WM_MBUTTONDOWN": 'WheelButton',
                       "WM_MBUTTONUP": 'WheelButton',
                       "WM_MOUSEWHEEL": 'Wheel'}

    MOUSE_ID_TO_EVENT_TYPE = {"WM_MOUSEMOVE": None,
                              "WM_LBUTTONDOWN": win32_events.get("WM_KEYDOWN"),
                              "WM_LBUTTONUP": win32_events.get("WM_KEYUP"),
                              "WM_RBUTTONDOWN": win32_events.get("WM_KEYDOWN"),
                              "WM_RBUTTONUP": win32_events.get("WM_KEYUP"),
                              "WM_MBUTTONDOWN": win32_events.get("WM_KEYDOWN"),
                              "WM_MBUTTONUP": win32_events.get("WM_KEYUP"),
                              "WM_MOUSEWHEEL": None}

    # TODO: use constants from win32con: VK_BACK, VK_TAB, VK_RETURN ...
    ID_TO_KEY = {1: 'LButton',  # win32con.VK_LBUTTON
                 2: 'RButton',  # win32con.VK_RBUTTON
                 3: 'Cancel',  # win32con.VK_CANCEL
                 4: 'MButton',  # win32con.VK_MBUTTON
                 5: 'XButton1',  # win32con.VK_XBUTTON1
                 6: 'XButton2',  # win32con.VK_XBUTTON2
                 7: 'Undefined1',
                 8: 'Back',
                 9: 'Tab',
                 10: 'Reserved1',
                 11: 'Reserved2',
                 12: 'Clear',  # win32con.VK_CLEAR
                 13: 'Return',  # win32con.VK_RETURN
                 14: 'Undefined2',
                 15: 'Undefined3',
                 16: 'SHIFT',  # win32con.VK_SHIFT
                 17: 'CONTROL',  # win32con.VK_CONTROL
                 18: 'Menu',  # win32con.VK_MENU
                 19: 'Pause',  # win32con.VK_PAUSE
                 20: 'Capital',
                 21: 'Kana',  # win32con.VK_KANA and win32con.VK_HANGUL
                 22: 'Undefined4',
                 23: 'Junja',  # win32con.VK_JUNJA
                 24: 'Final',  # win32con.VK_FINAL
                 25: 'Kanji',  # win32con.VK_KANJI and win32con.VK_HANJA
                 26: 'Undefined5',
                 27: 'Escape',
                 28: 'Convert',  # win32con.VK_CONVERT
                 29: 'NonConvert',  # win32con.VK_NONCONVERT
                 30: 'Accept',  # win32con.VK_ACCEPT
                 31: 'ModeChange',  # win32con.VK_MODECHANGE
                 32: 'Space',
                 33: 'Prior',
                 34: 'Next',
                 35: 'End',
                 36: 'Home',
                 37: 'Left',
                 38: 'Up',
                 39: 'Right',
                 40: 'Down',
                 41: 'Select',  # win32con.VK_SELECT
                 42: 'Print',  # win32con.VK_PRINT
                 43: 'Execute',  # win32con.VK_EXECUTE
                 44: 'Snapshot',
                 45: 'Insert',  # win32con.VK_INSERT
                 46: 'Delete',
                 47: 'Help',  # win32con.VK_HELP
                 48: '0',
                 49: '1',
                 50: '2',
                 51: '3',
                 52: '4',
                 53: '5',
                 54: '6',
                 55: '7',
                 56: '8',
                 57: '9',
                 58: 'Undefined6',
                 59: 'Undefined7',
                 60: 'Undefined8',
                 61: 'Undefined9',
                 62: 'Undefined10',
                 63: 'Undefined11',
                 64: 'Undefined12',
                 65: 'A',
                 66: 'B',
                 67: 'C',
                 68: 'D',
                 69: 'E',
                 70: 'F',
                 71: 'G',
                 72: 'H',
                 73: 'I',
                 74: 'J',
                 75: 'K',
                 76: 'L',
                 77: 'M',
                 78: 'N',
                 79: 'O',
                 80: 'P',
                 81: 'Q',
                 82: 'R',
                 83: 'S',
                 84: 'T',
                 85: 'U',
                 86: 'V',
                 87: 'W',
                 88: 'X',
                 89: 'Y',
                 90: 'Z',
                 91: 'Lwin',
                 92: 'Rwin',
                 93: 'App',
                 94: 'Reserved3',
                 95: 'Sleep',
                 96: 'Numpad0',
                 97: 'Numpad1',
                 98: 'Numpad2',
                 99: 'Numpad3',
                 100: 'Numpad4',
                 101: 'Numpad5',
                 102: 'Numpad6',
                 103: 'Numpad7',
                 104: 'Numpad8',
                 105: 'Numpad9',
                 106: 'Multiply',
                 107: 'Add',
                 108: 'Separator',  # win32con.VK_SEPARATOR
                 109: 'Subtract',
                 110: 'Decimal',
                 111: 'Divide',
                 112: 'F1',
                 113: 'F2',
                 114: 'F3',
                 115: 'F4',
                 116: 'F5',
                 117: 'F6',
                 118: 'F7',
                 119: 'F8',
                 120: 'F9',
                 121: 'F10',
                 122: 'F11',
                 123: 'F12',
                 124: 'F13',
                 125: 'F14',
                 126: 'F15',
                 127: 'F16',
                 128: 'F17',
                 129: 'F18',
                 130: 'F19',
                 131: 'F20',
                 132: 'F21',
                 133: 'F22',
                 134: 'F23',
                 135: 'F24',
                 136: 'Unassigned1',
                 137: 'Unassigned2',
                 138: 'Unassigned3',
                 139: 'Unassigned4',
                 140: 'Unassigned5',
                 141: 'Unassigned6',
                 142: 'Unassigned7',
                 143: 'Unassigned8',
                 144: 'Numlock',
                 145: 'Scroll',  # win32con.VK_SCROLL
                 146: 'OemSpecific1',
                 147: 'OemSpecific2',
                 148: 'OemSpecific3',
                 149: 'OemSpecific4',
                 150: 'OemSpecific5',
                 151: 'OemSpecific6',
                 152: 'OemSpecific7',
                 153: 'OemSpecific8',
                 154: 'OemSpecific9',
                 155: 'OemSpecific10',
                 156: 'OemSpecific11',
                 157: 'OemSpecific12',
                 158: 'OemSpecific13',
                 159: 'OemSpecific14',
                 160: 'Lshift',
                 161: 'Rshift',
                 162: 'Lcontrol',
                 163: 'Rcontrol',
                 164: 'Lmenu',
                 165: 'Rmenu',
                 166: 'BrowserBack',  # win32con.VK_BROWSER_BACK
                 167: 'BrowserForward',  # win32con.VK_BROWSER_FORWARD
                 168: 'BrowserRefresh',  # not defined in win32con
                 169: 'BrowserStop',  # not defined in win32con
                 170: 'BrowserSearch',  # not defined in win32con
                 171: 'BrowserFavourites',  # not defined in win32con
                 172: 'BrowserHome',  # not defined in win32con
                 173: 'Volume_mute',  # win32con.VK_VOLUME_MUTE
                 174: 'Volume_down',  # win32con.VK_VOLUME_DOWN
                 175: 'Volume_up',  # win32con.VK_VOLUME_UP
                 176: 'NextTrack',  # win32con.VK_MEDIA_NEXT_TRACK
                 177: 'PrevTrack',  # win32con.VK_MEDIA_PREV_TRACK
                 178: 'StopTrack',  # not defined in win32con
                 179: 'PlayPause',  # win32con.VK_MEDIA_PLAY_PAUSE
                 180: 'LaunchMail',  # not defined in win32con
                 181: 'MediaSelect',  # not defined in win32con
                 182: 'LaunchApp1',  # not defined in win32con
                 183: 'LaunchApp2',  # not defined in win32con
                 184: 'Reserved4',
                 185: 'Reserved5',
                 186: 'Oem_1',
                 187: 'Oem_Plus',
                 188: 'Oem_Comma',
                 189: 'Oem_Minus',
                 190: 'Oem_Period',
                 191: 'Oem_2',
                 192: 'Oem_3',
                 193: 'Reserved6',
                 194: 'Reserved7',
                 195: 'Reserved8',
                 196: 'Reserved9',
                 197: 'Reserved10',
                 198: 'Reserved11',
                 199: 'Reserved12',
                 200: 'Reserved13',
                 201: 'Reserved14',
                 202: 'Reserved15',
                 203: 'Reserved16',
                 204: 'Reserved17',
                 205: 'Reserved18',
                 206: 'Reserved19',
                 207: 'Reserved20',
                 208: 'Reserved21',
                 209: 'Reserved22',
                 210: 'Reserved23',
                 211: 'Reserved24',
                 212: 'Reserved25',
                 213: 'Reserved26',
                 214: 'Reserved27',
                 215: 'Reserved28',
                 216: 'Unassigned9',
                 217: 'Unassigned10',
                 218: 'Unassigned11',
                 219: 'Oem_4',
                 220: 'Oem_5',
                 221: 'Oem_6',
                 222: 'Oem_7',
                 223: 'Oem_8',  # not defined in win32cona
                 224: 'Reserved29',
                 225: 'OemSpecific15',
                 226: 'Oem_102',
                 227: 'OemSpecific16',
                 228: 'OemSpecific17',
                 229: 'ProcessKey',  # win32con.VK_PROCESSKEY
                 230: 'OemSpecific18',
                 231: 'VkPacket',  # win32con.VK_PACKET. It has a special processing in kbd_ll !
                 232: 'Unassigned12',
                 233: 'OemSpecific19',
                 234: 'OemSpecific20',
                 235: 'OemSpecific21',
                 236: 'OemSpecific22',
                 237: 'OemSpecific23',
                 238: 'OemSpecific24',
                 239: 'OemSpecific25',
                 240: 'OemSpecific26',
                 241: 'OemSpecific27',
                 242: 'OemSpecific28',
                 243: 'OemSpecific29',
                 244: 'OemSpecific30',
                 245: 'OemSpecific31',
                 246: 'Attn',  # win32con.VK_ATTN
                 247: 'CrSel',  # win32con.VK_CRSEL
                 248: 'ExSel',  # win32con.VK_EXSEL
                 249: 'ErEOF',  # win32con.VK_EREOF
                 250: 'Play',  # win32con.VK_PLAY
                 251: 'Zoom',  # win32con.VK_ZOOM
                 252: 'Noname',  # win32con.VK_NONAME
                 253: 'PA1',  # win32con.VK_PA1
                 254: 'OemClear',  # win32con.VK_OEM_CLEAR
                 1001: 'mouse left',  # mouse hotkeys
                 1002: 'mouse right',
                 1003: 'mouse middle',
                 1000: 'mouse move',  # single event hotkeys
                 1004: 'mouse wheel up',
                 1005: 'mouse wheel down',
                 1010: 'Ctrl',  # merged hotkeys
                 1011: 'Alt',
                 1012: 'Shift',
                 1013: 'Win'}

    event_types = {"WM_KEYDOWN": win32_events.get("WM_KEYDOWN"),  # WM_KEYDOWN for normal keys
                   "WM_KEYUP": win32_events.get("WM_KEYUP"),  # WM_KEYUP for normal keys
                   "WM_SYSKEYDOWN": win32_events.get("WM_SYSKEYDOWN"),  # WM_SYSKEYDOWN, is used for Alt key.
                   "WM_SYSKEYUP": win32_events.get("WM_SYSKEYUP"),  # WM_SYSKEYUP, is used for Alt key.
                   }

    def __init__(self):
        self.handler = None
        self.pressed_keys = []
        self.keyboard_id = None
        self.mouse_id = None
        self.mouse_is_hook = False
        self.keyboard_is_hook = False

    def _process_kbd_data(self, kb_data_ptr):
        """Process KbDllHookStruct data received from low level keyboard hook calls"""
        kbd = KbDllHookStruct.from_address(kb_data_ptr)
        current_key = None
        key_code = kbd.vkCode
        if key_code == win32_keys.get("VK_PACKET"):
            scan_code = kbd.scanCode
            current_key = six.unichr(scan_code)
        elif key_code in self.ID_TO_KEY:
            current_key = six.u(self.ID_TO_KEY[key_code])
        else:
            logger.error(f"_process_kbd_data, bad key_code: {key_code}")

        return current_key

    def _process_kbd_msg_type(self, event_code, current_key):
        """Process event codes from low level keyboard hook calls"""
        event_type = None
        event_code_word = 0xFFFFFFFF & event_code
        if event_code_word in self.event_types:
            event_type = self.event_types[event_code_word]
        else:
            logger.error(f"_process_kbd_msg_type, bad event_type: {event_type}")

        if event_type == win32_events.get("WM_KEYDOWN") and current_key not in self.pressed_keys:
            self.pressed_keys.append(current_key)
        elif event_type == 'key up':
            if current_key in self.pressed_keys:
                self.pressed_keys.remove(current_key)
            else:
                logger.error(f"_process_kbd_msg_type, can't remove a key: {current_key}")
        return event_type

    def _keyboard_ll_hdl(self, code, event_code, kb_data_ptr):
        """Execute when a keyboard low level event has been triggered"""
        try:
            # The next hook in chain must be always called
            res = windll.user32.CallNextHookEx(self.keyboard_id, code, event_code, kb_data_ptr)
            if not self.handler:
                return res

            current_key = self._process_kbd_data(kb_data_ptr)
            event_type = self._process_kbd_msg_type(event_code, current_key)
            event = KeyboardEvent(current_key, event_type, self.pressed_keys)
            self.handler(event)

        except Exception:
            logger.error(f"_keyboard_ll_hdl, {sys.exc_info()[0]}")
            logger.error(f"_keyboard_ll_hdl, code {code}, event_code {event_code}")
            raise

        return res

    def _mouse_ll_hdl(self, code, event_code, mouse_data_ptr):
        """Execute when a mouse low level event has been triggerred"""
        try:
            # The next hook in chain must be always called
            res = windll.user32.CallNextHookEx(self.mouse_id, code, event_code, mouse_data_ptr)
            if not self.handler:
                return res

            current_key = None
            event_code_word = 0xFFFFFFFF & event_code
            if event_code_word in self.MOUSE_ID_TO_KEY:
                current_key = self.MOUSE_ID_TO_KEY[event_code_word]

            event_type = None
            if current_key != 'Move':
                if event_code in self.MOUSE_ID_TO_EVENT_TYPE:
                    event_type = self.MOUSE_ID_TO_EVENT_TYPE[event_code]

                # Get the mouse position: x and y
                ms = MsLLHookStruct.from_address(mouse_data_ptr)
                event = MouseEvent(current_key, event_type, ms.pt.x, ms.pt.y)
                self.handler(event)

        except Exception:
            logger.error(f"_mouse_ll_hdl, {sys.exc_info()[0]}")
            logger.error(f"_mouse_ll_hdl, code {code}, event_code {event_code}")
            raise

        return res

    def hook(self, keyboard=True, mouse=False):
        """Hook mouse and/or keyboard events"""
        if not (mouse or keyboard):
            return

        self.mouse_is_hook = mouse
        self.keyboard_is_hook = keyboard

        if self.keyboard_is_hook:
            @hook_cb
            def _kbd_ll_cb(ncode, wparam, lparam):
                """Forward the hook event to ourselves"""
                return self._keyboard_ll_hdl(ncode, wparam, lparam)

            self.keyboard_id = windll.user32.SetWindowsHookExW(
                wh_keyboard_ll, _kbd_ll_cb, windll.kernel32.GetModuleHandleA(None), 0)

        if self.mouse_is_hook:
            @hook_cb
            def _mouse_ll_cb(code, event_code, mouse_data_ptr):
                """Forward the hook event to ourselves"""
                return self._mouse_ll_hdl(code, event_code, mouse_data_ptr)

            self.mouse_id = windll.user32.SetWindowsHookExA(
                wh_mouse_ll, _mouse_ll_cb, windll.kernel32.GetModuleHandleA(None), 0)
        self.listen()

    def unhook_mouse(self):
        """Unhook mouse events"""
        if self.mouse_is_hook:
            self.mouse_is_hook = False
            windll.user32.UnhookWindowsHookEx(self.mouse_id)

    def unhook_keyboard(self):
        """Unhook keyboard events"""
        if self.keyboard_is_hook:
            self.keyboard_is_hook = False
            windll.user32.UnhookWindowsHookEx(self.keyboard_id)

    def stop(self):
        """Stop the listening loop"""
        self.unhook_keyboard()
        self.unhook_mouse()

    def is_hooked(self):
        """Verify if any of hooks are active"""
        return self.mouse_is_hook or self.keyboard_is_hook

    def _process_win_msgs(self):
        """Peek and process queued windows messages"""
        message = wintypes.MSG()
        while True:
            res = windll.user32.PeekMessageW(pointer(message), 0, 0, 0, pm_remove)
            if not res:
                break
            if message.message == wm_quit:
                self.stop()
                sys.exit(0)
            else:
                windll.user32.TranslateMessage(byref(message))
                windll.user32.DispatchMessageW(byref(message))

    def listen(self):
        """Listen for events"""
        atexit.register(windll.user32.UnhookWindowsHookEx, self.keyboard_id)
        atexit.register(windll.user32.UnhookWindowsHookEx, self.mouse_id)

        while self.is_hooked():
            self._process_win_msgs()
            time.sleep(0.02)


if __name__ == "__main__":

    def on_event(args):
        """Callback for keyboard and mouse events"""
        if isinstance(args, KeyboardEvent):
            if args.current_key == 'A' and args.event_type == win32_events.get(
                    "WM_KEYDOWN") and 'Lcontrol' in args.pressed_key:
                print("Ctrl + A was pressed")

            if args.current_key == 'K' and args.event_type == win32_events.get("WM_KEYDOWN"):
                print("K was pressed")

            if args.current_key == 'M' and args.event_type == win32_events.get(
                    "WM_KEYDOWN") and 'U' in args.pressed_key:
                hk.unhook_mouse()
                print("Unhook mouse")

            if args.current_key == 'K' and args.event_type == win32_events.get(
                    "WM_KEYDOWN") and 'U' in args.pressed_key:
                hk.unhook_keyboard()
                print("Unhook keyboard")

        if isinstance(args, MouseEvent):
            if args.current_key == 'RButton' and args.event_type == win32_events.get("WM_KEYDOWN"):
                print("Right button pressed at ({0}, {1})".format(args.mouse_x, args.mouse_y))

            if args.current_key == 'WheelButton' and args.event_type == win32_events.get("WM_KEYDOWN"):
                print("Wheel button pressed")


    hk = Hook()
    hk.handler = on_event
    hk.hook(keyboard=True, mouse=True)
