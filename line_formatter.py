class LineFormatter:
    def __init__(self, separator: str, old: str, new: str, is_both_ends: bool = True):
        self.old = old
        self.new = new
        self.separator = separator
        self.is_both_ends = is_both_ends

    def formatting(self, line: str):
        modified_lines = []
        _start = line[0]
        _end = line[-1]
        for part in line[1:-2].split(self.separator):
            _start_index = int(part.startswith(self.old))
            if self.is_both_ends:
                _end_index = -int(part.endswith(self.old)) - 1
                modified = f"{self.new}{part[_start_index:_end_index]}{self.new}"
            else:
                modified = f"{self.new}{part[_start_index:]}"
            modified_lines.append(modified)
        return f"{_start}{self.separator.join(modified_lines)}{_end}"
