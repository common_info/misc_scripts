from __future__ import annotations

import os
from pathlib import Path
from re import Pattern, compile, match
from typing import TypeAlias, Iterable, Match, Iterator, Mapping

from loguru import logger

from init_logger import configure_custom_logging

StrPath: TypeAlias = str | Path


class LogFileProduct:
    def __init__(self, path: StrPath, content: Iterable[str] = None):
        if isinstance(path, str):
            path: Path = Path(path)
        if content is None:
            content: list[str] = []
        self._path: Path = path
        self._content: list[str] = [*content]

    def __str__(self):
        return f"{self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path}, {self._content})>"

    def read(self):
        with open(self._path, "r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def __bytes__(self):
        return b"".join([_.encode() for _ in self._content])

    def write(self):
        with open(self._path, "wb") as f:
            f.write(bytes(self))

    def __iter__(self) -> Iterator[str]:
        return iter(self._content)

    def __len__(self) -> int:
        return len(self._content)

    def __getitem__(self, item) -> str | list[str]:
        return self._content[item]

    def __setitem__(self, key, value):
        self._content[key] = value

    def insert_line(self, index: int, line: str):
        self._content.insert(index, line)

    def get_log_lines(self) -> list[LogFileLine]:
        return [LogFileLine(self, index) for index in range(len(self))]

    def iter_log_lines(self) -> Iterator[LogFileLine]:
        return iter([LogFileLine(self, index) for index in range(len(self))])

    def add_lines(self, rapper: str, new: Iterable[str]):
        _lines: list[str] = self._content
        for index, _ in enumerate(iter(self)):
            if _ == rapper:
                for _new in new:
                    _lines.insert(index, _new)
        self._content = _lines
        self.write()

    def modify_lines(self, rapper: str, new: Iterable[str]):
        for line in self.iter_log_lines():
            # line.replace_lines().add_lines(rapper, new).update()
            line.replace_lines().update()
        return self


class FileDirectory:
    def __init__(self, path: StrPath):
        if isinstance(path, str):
            path: Path = Path(path)
        self._path: Path = path
        if not self._path.is_dir():
            raise KeyError("Invalid path")

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    def recursive_files(self) -> list[Path]:
        return [Path(root).joinpath(file) for root, _, files in os.walk(self._path) for file in files]

    def iter_recursive_files(self) -> Iterator[str]:
        return iter(os.path.join(root, file) for root, _, files in os.walk(self._path) for file in files)

    def recursive_subfolders(self) -> list[Path]:
        return [Path(root).joinpath(subdir) for root, subdirs, _ in os.walk(self._path) for subdir in subdirs]

    def recursive_file_names(self) -> tuple[str, ...]:
        return tuple(map(lambda x: Path(x).name, self.recursive_files()))

    def iter_recursive_file_names(self) -> Iterator[str]:
        return iter(map(lambda x: Path(x).name, self.recursive_files()))

    def repr_recursive_files(self):
        return "\n".join([str(path) for path in self.recursive_files()])

    def repr_recursive_subfolders(self) -> str:
        return "\n".join([str(path) for path in self.recursive_subfolders()])

    def repr_recursive_file_names(self) -> str:
        return "\n".join([str(path) for path in self.recursive_file_names()])

    def __iter__(self) -> Iterator[LogFileProduct]:
        return iter(LogFileProduct(_) for _ in self.iter_recursive_files())


class LogFileLine:
    def __init__(self, log_file: LogFileProduct, index: int = -1):
        self._log_file: LogFileProduct = log_file
        self._index: int = index
        self._line: str = self._log_file[self._index]

    def modify_cdr_fields(self):
        pattern: Pattern = compile(r"(\d+)\.?\s([\w\s]+)\s-\s(.*)")
        _m: Match = match(pattern, self._line)
        if _m:
            self._line = f"| {_m.group(1)} | {_m.group(2)} | {_m.group(3)} | |\n"
        return self

    def modify_cdr_errors(self):
        pattern: Pattern = compile(r"\*\s(\d+)\s-\s(.*)")
        _m: Match = match(pattern, self._line)
        if _m:
            self._line = f"| {_m.group(1)} | {_m.group(2).capitalize()}. |\n"
        return self

    def replace(self, replacement: Mapping[str, str]):
        for k, v in replacement.items():
            self._line = self._line.replace(k, v)
        return self

    def replace_lines(self):
        replacement: dict[str, str] = {
            "Cell ID": "CellID",
            "Cell Id": "CellID",
            "id Соты | |": "Идентификатор соты. | int/hex |",
            "ID соты | |": "Идентификатор соты. | int/hex |",
            "глобально уникальный временный ID абонента | |":
                "Глобальный уникальный временный идентификатор абонента. | string |",
            "глобально уникальный временный id абонента":
                "Глобальный уникальный временный идентификатор абонента. | string |",
            "IMEI абонента | |": "Номер IMEI устройства. | string |",
            "IMSI | IMSI абонента | |": "IMSI | Номер IMSI абонента. | string |",
            "MSISDN | MSISDN абонента | |": "MSISDN | Номер MSISDN абонента. | string |",
            "| SGW IP | SGW IP | |": "| SGW IP | IP-адрес узла SGW. | ip |",
            "идентификатор PLMN | |": "Идентификатор PLMN. | string |",
            "Код Tracking Area | |": "Код области отслеживания. | int |",
            "идентификатор S1 контекста на стороне MME | |": "Идентификатор S1-контекста на узле MME. | int |",
            "идентификатор S1 контекста на стороне ENB | |": "Идентификатор S1-контекста на узле eNodeB. | int |",
            "ENB UE ID": "eNodeB UE ID",
            "длительность процедуры, связанной с cdr в ms | |": "Длительность процедуры. | int<br>мс |",
            "Длительность процедуры, связанной с CDR в ms | |": "Длительность процедуры. | int<br>мс |",
            "| SGW TEID | SGW TEID | |": "| SGW TEID | Идентификатор конечной точки туннеля на узле SGW. | string |",
            "Access Point Name | |": "Имя точки доступа. | string |",
            "DateTime | дата и время в формате `YYYY-MM-DD HH:MM:SS` | |":
                "Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime |",
            "DateTime | Дата и время в формате `YYYY-MM-DD HH:MM:SS` | |":
                "Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime |",
            "| 2 | Event | Событие или процедура, которая привела к формированию CDR: | |":
                "| 2 | [Event](#event) | Событие или процедура, сформировавшая запись. | string |",
            "enodeb": "eNodeB",
            "Запрошен неизвестный бирер.": "Запрошена неизвестная bearer-служба.",
            "Нет такого бирера.": "Указанная bearer-служба не найдена.",
            "Нет биреров, подходящих для модификации.": "Подходящие для модификации bearer-службы не найдены.",
            "Ошибка при модификации бирера.": "Ошибка при модификации bearer-службы.",
            "sgw": "SGW",
            "pgw": "PGW",
            "mme": "MME",
            "nb-iot": "NB-IoT",
            "imsi": "IMSI",
            "imeisv": "IMEISV",
            "srvcc": "SRVCC",
            "msc": "MSC",
            "sgsn": "SGSN",
            "utran": "UTRAN",
            "tau": "TAU",
            "guti": "GUTI",
            "ppf": '<abbr title="Page Proceed Flag">PPF</abbr>',
            "paging": "Paging",
            "tac": "TAC",
            "pdn": "PDN",
            "eir": "EIR",
            "imei": "IMEI",
            "scef": "SCEF",
            "eps": "EPS"
        }
        return self.replace(replacement)

    def get_upper(self, words: Iterable[str]):
        for word in words:
            self._line.replace(word, word.upper())

    def add_lines(self, rapper: str, new: Iterable[str]):
        if self._line == rapper:
            _ = "\n".join(new)
            self._line = "\n".join((self._line, _))
        return self

    def update(self):
        self._log_file[self._index] = self._line


@logger.catch
@configure_custom_logging("logs_md")
def main():
    dir_path: str = r"C:\Users\tarasov-a\PycharmProjects\mme\content\common\data_records\cdr"
    file_directory: FileDirectory = FileDirectory(dir_path)
    log_file: LogFileProduct
    for log_file in iter(file_directory):
        log_file.read()
        _: list[str] = [
            ""
            "| N | Description |",
            "|---|-------------|\n"
        ]
        base_line: str = "Локальные коды ошибок:\n"
        log_file.modify_lines(base_line, _)
        log_file.write()
        logger.info(str(log_file))
        # log_file.add_lines("Локальные коды ошибок:\n", _)
        log_file.write()


if __name__ == '__main__':
    main()
