import re
from re import Match
from typing import Iterable, Optional, Pattern

from md_table import MarkdownColumnHeader, MarkdownHeader, MarkdownRow, MarkdownTable


class MarkdownParser:
    def __init__(self, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._lines: list[str] = lines
        self._md_table: Optional[MarkdownTable] = None

    @property
    def lines(self):
        return self._lines

    @lines.setter
    def lines(self, value):
        self._lines = value

    @property
    def md_table(self):
        return self._md_table

    @md_table.setter
    def md_table(self, value):
        self._md_table = value

    def __getitem__(self, item):
        return self._lines.__getitem__(item)

    def __iter__(self):
        return iter(self._lines)

    def convert(self):
        # md_header: MarkdownHeader = MarkdownHeader.from_string(self[0])
        md_header: MarkdownHeader = MarkdownColumnHeader()
        md_rows: list[MarkdownRow] = [MarkdownRow.from_string(line, index) for index, line in enumerate(self)]
        self._md_table = MarkdownTable(md_header, md_rows)


class MarkdownParserLine:
    _default_pattern: Pattern = re.compile(r"Значение по умолчанию\s+.\s+(\w+)\.")
    _type_pattern: Pattern = re.compile(r"Тип\s+.\s+(\w+)\.")
    _type_list_pattern: Pattern = re.compile(r"Тип\s+.\s+(\w+),\s.*\s(\w+)\.")
    _note_pattern: Pattern = re.compile(f"Примечание\.(.*)")

    def __init__(self):
        self._line: Optional[str] = None
        self._md_header: MarkdownHeader = MarkdownColumnHeader()
        self._parts: list[str] = []

    @property
    def line(self):
        return self._line

    @line.setter
    def line(self, value):
        self._line = value

    @property
    def md_header(self):
        return self._md_header

    @md_header.setter
    def md_header(self, value):
        self._md_header = value

    @property
    def parts(self):
        return self._parts

    @parts.setter
    def parts(self, value):
        self._parts = value

    def _get_parts(self):
        return self._line.split("\t")

    def __iter__(self):
        return iter(self._md_header)

    def _get_parameter(self):
        return self._get_parts().__getitem__(0)

    def _get_ompr(self):
        return self._get_parts().__getitem__(1).split("/")

    def _get_om(self):
        return self._get_ompr().__getitem__(0)

    def _get_pr(self):
        return self._get_ompr().__getitem__(1)

    def _get_text(self):
        return self._get_parts().__getitem__(2)

    def _get_default(self):
        return re.fullmatch(self._default_pattern, self._get_text())

    def _get_type(self):
        match_list: Match = re.fullmatch(self._type_list_pattern, self._get_text())
        if match_list:
            return match_list
        else:
            match: Match = re.fullmatch(self._type_pattern, self._get_text())
            return match

    def _get_note(self):
        return re.fullmatch(self._note_pattern, self._get_text())

    def _get_description(self):
        return MarkdownParserSplit.parse_line(self._get_text())


class MarkdownParserSplit:
    _default_pattern: Pattern = re.compile(r"Значение по умолчанию\s+.\s+(\w+)\.")
    _type_pattern: Pattern = re.compile(r"Тип\s+.\s+(\w+)\.")
    _note_pattern: Pattern = re.compile(f"Примечание\.(.*)")

    _patterns = (_default_pattern.pattern, _type_pattern.pattern, _note_pattern.pattern)

    @staticmethod
    def replace(line: str, substrings: Iterable[str] = None):
        if substrings is None:
            return line
        for substring in substrings:
            line = line.replace(substring)
        return line.strip()

    @classmethod
    def parse_line(cls, line: str):
        return cls.replace(line, cls._patterns)
