from typing import Iterable, Iterator, NamedTuple, Union

from loguru import logger


class MarkdownColumn(NamedTuple):
    name: str
    col_idx: int

    def __str__(self):
        return f" {self.name} |"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.name}, {self.col_idx})>"


class MarkdownRow:
    def __init__(
            self,
            row_idx: int,
            md_columns: Iterable[MarkdownColumn] = None,
            values: Iterable[str] = None):
        if md_columns is None:
            md_columns: list[MarkdownColumn] = []
        if values is None:
            values: list[str] = []
        self._row_idx: int = row_idx
        self._md_columns: list[MarkdownColumn] = [*md_columns]
        self._values: list[str] = values

    def __str__(self):
        _column_str: str = "".join([str(column) for column in self._md_columns])
        return f"|{_column_str}"

    def __repr__(self):
        _column_str: str = "".join([str(column) for column in self._md_columns])
        return f"<{self.__class__.__name__}: {self._row_idx}>|{_column_str}"

    @classmethod
    def from_string(cls, line: str, row_idx: int):
        _elements: list[str] = line.strip("|").split("|")
        return cls(row_idx, [MarkdownColumn(_el.strip(), index) for index, _el in enumerate(_elements)])

    @property
    def row_idx(self):
        return self._row_idx

    @row_idx.setter
    def row_idx(self, value):
        self._row_idx = value

    @property
    def md_columns(self):
        return self._md_columns

    @md_columns.setter
    def md_columns(self, value):
        self._md_columns = value

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, value):
        self._values = value

    def __len__(self):
        return len(self._md_columns)

    def __bool__(self):
        return len(self) > 0

    def __iter__(self):
        return iter(self._md_columns)

    def iter_values(self):
        return iter(self._values)

    def __getitem__(self, item):
        return self._md_columns.__getitem__(item)

    def __setitem__(self, key, value):
        self._md_columns.__setitem__(key, value)

    def __hash__(self):
        return hash(self._row_idx)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._row_idx == other._row_idx
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._row_idx == other._row_idx
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self._row_idx > other._row_idx
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self._row_idx < other._row_idx
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self._row_idx >= other._row_idx
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self._row_idx <= other._row_idx
        else:
            return NotImplemented

    def sort(self) -> list[MarkdownColumn]:
        return sorted(self._md_columns, key=lambda x: x.col_idx)

    def names(self) -> list[str]:
        return [column.name for column in self.sort()]

    def _recalculate(self):
        self._md_columns = [MarkdownColumn(item.name, index) for index, item in enumerate(self._md_columns)]

    def __add__(self, other):
        if not isinstance(other, MarkdownColumn):
            return NotImplemented
        if other.col_idx < len(self):
            self._md_columns.insert(other.col_idx, other)
            self._recalculate()
        else:
            _md_column: MarkdownColumn = MarkdownColumn(other.name, len(self))
            self._md_columns.append(_md_column)
        return

    __radd__ = __add__
    __iadd__ = __add__

    def __sub__(self, other):
        if not isinstance(other, MarkdownColumn):
            return NotImplemented
        if other.name not in self.names():
            logger.error(f"Column {other.name} is not found")
            raise KeyError
        else:
            self._md_columns.remove(other)
            self._recalculate()

    def md_cells(self):
        return [MarkdownCell(self._row_idx, index, value) for index, value in enumerate(self.iter_values())]


class MarkdownHeader(MarkdownRow):
    def __init__(self, md_columns: Iterable[MarkdownColumn] = None):
        row_idx: int = -1
        super().__init__(row_idx, md_columns)

    @classmethod
    def from_string(cls, line: str, **kwargs):
        _elements: list[str] = line.strip("|").split("|")
        return cls([MarkdownColumn(_el.strip(), index) for index, _el in enumerate(_elements)])

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        elif isinstance(other, super().__class__):
            return False
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        elif isinstance(other, super().__class__):
            return True
        else:
            return NotImplemented


class MarkdownCell:
    def __init__(
            self,
            row_idx: Union[MarkdownRow, int],
            col_idx: Union[MarkdownColumn, int],
            value: str = None):
        if isinstance(row_idx, MarkdownRow):
            row_idx: int = row_idx.row_idx
        if isinstance(col_idx, MarkdownColumn):
            col_idx: int = col_idx.col_idx
        self._row_idx: int = row_idx
        self._col_idx: int = col_idx
        self._value: str = value

    @property
    def row_idx(self):
        return self._row_idx

    @row_idx.setter
    def row_idx(self, value):
        self._row_idx = value

    @property
    def col_idx(self):
        return self._col_idx

    @col_idx.setter
    def col_idx(self, value):
        self._col_idx = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v


class MarkdownTable:
    def __init__(self, md_header: MarkdownHeader, md_rows: Iterable[MarkdownRow] = None):
        if md_rows is None:
            md_rows: list[MarkdownRow] = []
        self._md_header: MarkdownHeader = md_header
        self._md_rows: list[MarkdownRow] = [*md_rows]

    def __str__(self):
        _line: str = "- | " * len(self._md_header)
        _separator: str = f"|{_line}"
        _header: str = str(self._md_header)
        _rows: list[str] = [str(row) for row in self._md_rows]
        return "\n".join([_header, _separator, *_rows])

    @property
    def md_header(self):
        return self._md_header

    @md_header.setter
    def md_header(self, value):
        self._md_header = value

    @property
    def md_rows(self):
        return self._md_rows

    @md_rows.setter
    def md_rows(self, value):
        self._md_rows = value

    def __iter__(self) -> Iterator[MarkdownRow]:
        return iter(self._md_rows)

    def iter_columns(self) -> Iterator[MarkdownColumn]:
        return iter(row.__getitem__(row_idx) for row in self._md_rows for row_idx in range(len(self._md_rows)))

    def iter_rows(self) -> Iterator[MarkdownRow]:
        return iter(self)

    def iter_cells(self) -> Iterator[MarkdownCell]:
        yield [
            MarkdownCell(row.row_idx, col.col_idx, row.values.__getitem__(col.col_idx))
            for row in self._md_rows for col in row.md_columns]

    @property
    def md_cells(self) -> list[MarkdownCell]:
        return list(self.iter_cells())

    def __contains__(self, item):
        if isinstance(item, MarkdownRow):
            return item in self._md_rows
        elif isinstance(item, MarkdownHeader):
            return item == self._md_header
        elif isinstance(item, MarkdownCell):
            return item in self.md_cells
        else:
            return False

    def __add__(self, other):
        if isinstance(other, MarkdownRow):
            self._md_rows.append(other)
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def __getitem__(self, item):
        return self._md_rows.__getitem__(item)

    @property
    def names(self):
        return [column.name for column in self._md_header]

    def get_column(self, column_name: str):
        if column_name not in self.names:
            logger.error(f"{column_name} is not found")
            raise KeyError
        index: int = self._md_header.names().index(column_name)
        return [row.md_cells().__getitem__(index).value for row in self._md_rows]


class MarkdownColumnHeader(MarkdownHeader):
    def __init__(self):
        _parameter: MarkdownColumn = MarkdownColumn("parameter", 0)
        _description: MarkdownColumn = MarkdownColumn("description", 1)
        _type: MarkdownColumn = MarkdownColumn("type", 2)
        _default: MarkdownColumn = MarkdownColumn("default", 3)
        _om: MarkdownColumn = MarkdownColumn("om", 4)
        _pr: MarkdownColumn = MarkdownColumn("pr", 5)
        _version: MarkdownColumn = MarkdownColumn("version", 6)
        _md_columns: tuple[MarkdownColumn, ...] = (_parameter, _description, _type, _default, _om, _pr, _version)
        super().__init__(_md_columns)
