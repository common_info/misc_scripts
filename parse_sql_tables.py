import re
import textwrap
from enum import Enum, auto
from re import Pattern
from typing import Iterable, Optional, NamedTuple


class ParserType(Enum):
    SHORT = auto
    FULL = auto


class _SQLParametersParser:
    _separator_pattern: Pattern = re.compile(r"^\+.*\+$")

    def __init__(self, parser_type: ParserType = ParserType.SHORT):
        self._parser_type: ParserType = parser_type

    def __str__(self):
        return f"{self.__class__.__name__}: {self._parser_type.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._parser_type.name})>"

    @classmethod
    def __call__(cls):
        return cls()

    def is_separator(self, line: str):
        return re.match(self._separator_pattern, line) is not None

    def parse_line(self, line: str):
        """| NID     | int(11)      | NO   | PRI | NULL    | auto_increment |"""
        if not line:
            return []
        if self.is_separator(line):
            return []
        _items: list[str] = line[1:-1].split(" | ")
        # print(f"_items = {list(map(lambda x: x.strip(), _items))}")
        return list(map(lambda x: x.strip(), _items))


class SQLTable:
    _headers: list[str] = ["Field", "Type", "Null", "Key", "Default", "Extra"]

    def __init__(self, lines: Iterable[str] = None):
        if lines is None:
            lines = []
        self._lines: list[str] = lines

    @property
    def sql_parser(self):
        return _SQLParametersParser()

    @property
    def _heading_lines(self) -> list[int]:
        return [index for index, line in enumerate(self) if self.sql_parser.is_separator(line)]

    @property
    def _header_line(self):
        for index, line in enumerate(self):
            if self.sql_parser.parse_line(line) == self._headers:
                return index

    @property
    def _params_lines_indexes(self) -> list[int]:
        return [i for i in range(len(self)) if i not in (*self._heading_lines, self._header_line)]

    @property
    def _params_lines(self) -> list[str]:
        return [self[index] for index in self._params_lines_indexes]

    def __str__(self):
        return f"{self.__class__.__name__}: {[l for l in self._lines]}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._lines})>"

    def __iter__(self):
        return iter(self._lines)

    def __getitem__(self, item):
        return self._lines.__getitem__(item)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._params_lines == other._params_lines
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._params_lines != other._params_lines
        else:
            return NotImplemented

    def __len__(self):
        return len(self._lines)

    def __bool__(self):
        return len(self) > 0

    @property
    def lines(self):
        return self._lines

    @lines.setter
    def lines(self, value):
        self._lines = value

    def parse(self):
        _sql_params = []
        for line in self._params_lines:
            print(self.sql_parser.parse_line(line))

            sql_field, sql_type, sql_null, sql_key, sql_default, sql_extra = self.sql_parser.parse_line(line)
            _sql_params.append(_SQLParameter(sql_field, sql_type, sql_null, sql_key, sql_default, sql_extra))
        return _sql_params


class _SQLParameter(NamedTuple):
    sql_field: str
    sql_type: str
    sql_null: Optional[str] = None
    sql_key: Optional[str] = None
    sql_default: Optional[str] = None
    sql_extra: Optional[str] = None

    def attrs(self):
        return self._asdict().keys()


class SQLTableParsed:

    def __init__(self, sql_table: SQLTable, sql_parameters: Iterable[_SQLParameter] = None):
        if sql_parameters is None:
            sql_parameters = []
        self._sql_table: SQLTable = sql_table
        self._sql_parameters: list[_SQLParameter] = sql_parameters

    def __iter__(self):
        return iter(self._sql_parameters)

    def __getitem__(self, item):
        return self._sql_parameters.__getitem__(item)

    def __len__(self):
        return len(self._sql_parameters)

    def get_param(self, attr: str):
        print(f"attr = {attr}")
        for sql_parameter in self._sql_parameters:
            print(f"sql_parameter = {sql_parameter}")
        return [getattr(sql_parameter, attr) for sql_parameter in self._sql_parameters]


def main():
    lines = textwrap.dedent("""\
| NID        | int(11)      | NO   | PRI | NULL    | auto_increment |
| NWL_ID     | int(11)      | YES  | MUL | NULL    |                |
| STRMASK    | varchar(20)  | NO   |     | NULL    |                |
| STRCOUNTRY | varchar(200) | NO   |     | NULL    |                |
| STRNETWORK | varchar(200) | NO   |     | NULL    |                |""").split("\n")
    for line in lines:
        print(f"line = {line}")
    sql_table: SQLTable = SQLTable(lines)
    print(f"_params_lines_indexes = {sql_table._params_lines_indexes}")
    sql_parameters: list[_SQLParameter] = sql_table.parse()
    print(SQLTableParsed(sql_table, sql_parameters).get_param("Field"))

if __name__ == '__main__':
    main()
