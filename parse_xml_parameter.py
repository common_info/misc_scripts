from typing import Iterable, Optional, NamedTuple

import lxml.etree
from loguru import logger
from lxml.etree import ElementBase, fromstring
from user_input import multiline_iterable_input
from init_logger import configure_custom_logging


class EmptyXMLText(ValueError):
    """Empty XML lines."""


class InvalidElementLevel(ValueError):
    """Element level must be positive integer."""


class CustomXMLElement(NamedTuple):
    element: ElementBase
    level: int

    def parent(self) -> ElementBase:
        return self.element.getparent()

    def children(self) -> list[ElementBase]:
        return self.element.getchildren()

    def attributes(self) -> dict:
        return self.element.attrib.items()

    def custom_xml_parent(self):
        if self.parent() is not None:
            return CustomXMLElement(self.parent(), self.level - 1)
        else:
            return NullCustomXMLElement

    def custom_xml_children(self):
        return [CustomXMLElement(child, self.level + 1) for child in self.children()]

    def __str__(self):
        child_tags: str = "\n".join((child.tag for child in self.children()))
        return f"<level {self.level}> {self.element.tag}\nAttributes:{tuple(self.attributes())} Children:\n{child_tags}"


class NullCustomXMLElement(CustomXMLElement):
    element: ElementBase = None
    level: int = None


class CustomXMLParser:
    def __init__(self, text: Optional[Iterable[str]] = None):
        self._text = text
        self._root: Optional[ElementBase] = None
        self._dict_xml_elements: dict[str, CustomXMLElement] = dict()
        self._init()

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @property
    def line_text(self):
        if self.text is not None:
            return "\n".join(self.text)
        else:
            logger.error("empty xml")
            raise EmptyXMLText

    @property
    def root(self) -> Optional[ElementBase]:
        return self._root

    @root.setter
    def root(self, value):
        self._root = value

    def xml_elements(self, element: ElementBase, level: int = 0):
        custom_xml_element: CustomXMLElement = CustomXMLElement(element, level)
        self._dict_xml_elements[element.tag] = custom_xml_element
        for child in element.getchildren():
            self.xml_elements(child, level + 1)

    def _init(self):
        self.root = fromstring(self.line_text)
        self.xml_elements(self.root, 0)

    def get_custom_xml_by_level(self, level: int = 0):
        if not isinstance(level, int) or level < 0:
            logger.error("invalid level")
            raise InvalidElementLevel
        return tuple(map(lambda x: str(x), filter(lambda x: x.level == level, self._dict_xml_elements.values())))

@configure_custom_logging
def main():
    user_input = multiline_iterable_input()
    custom_xml_parser: CustomXMLParser = CustomXMLParser(user_input)

    logger.info(lxml.etree.tostring(custom_xml_parser.root, encoding=str, pretty_print=True))
    logger.info(custom_xml_parser.get_custom_xml_by_level(1))


if __name__ == "__main__":
    main()
