from pathlib import Path
from typing import Iterable, Union
from loguru import logger

from init_logger import configure_custom_logging
from user_input import multiline_iterable_input



class FilePathFactory:
    def __init__(self, paths: Iterable[Path]):
        self._paths: list[Path] = [*paths]

    def _check_paths(self):
        if len(self._paths) == 1:
            if self._paths[0].is_dir():
                return FilePathDirectory(self._paths[0])
            else:
                logger.error("Not a directory")
                raise ValueError
        else:
            return FilePathFactory(self._paths)

    @property
    def files(self):
        return self._check_paths().files



class FilePathFiles:
    def __init__(self, files: Iterable[str] = None):
        if files is None:
            files = []
        self._files: list[str] = [*files]

    @property
    def files(self):
        return self._files

    @files.setter
    def files(self, value):
        self._files = value

    def add_file(self, file: str):
        if file not in self:
            self.files.append(file)

    def add_files(self, files: Iterable[str]):
        for file in files:
            self.add_file(file)

    def __contains__(self, item):
        return item in self.files

    def __getitem__(self, item):
        return self.files.__getitem__(item)

    def __len__(self):
        return len(self.files)

    def __bool__(self):
        return len(self) != 0



class FilePathDirectory:
    def __init__(self, directory: Union[str, Path], extensions: Iterable[str] = None):
        if isinstance(directory, str):
            directory: Path = Path(directory)
        if extensions is None:
            extensions = []
        try:
            self._directory: Path = directory.resolve(strict=True)
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, General handling error, {e.strerror}")
            raise
        self._extensions: list[str] = [*extensions]

    @property
    def directory(self):
        return self._directory

    @property
    def extensions(self):
        return self._extensions

    @extensions.setter
    def extensions(self, value):
        self._extensions = value

    def add_extensions(self, extensions: Iterable[str]):
        self.extensions.extend(extensions)

    @property
    def files(self):
        if self.extensions:
            return tuple(filter(lambda x: x.suffix in self.extensions, self.directory.iterdir()))
        else:
            return tuple(self.directory.iterdir())



class TextFileRecoder:
    def __init__(self, codec: str, files: Iterable[Union[str, Path]] = None):
        if files is None:
            files = []
        self._codec: str = codec
        self._files: list[str] = files

    @property
    def codec(self):
        return self._codec

    @codec.setter
    def codec(self, value):
        self._codec = value

    @property
    def files(self):
        return self._files

    @files.setter
    def files(self, value):
        self._files = value

    def recode(self):
        if not bool(self):
            logger.error(f"No files to recode")
            raise ValueError
        try:
            for file in self.files:
                logger.error(file)
                with open(file, "rb") as f:
                    content: bytes = f.read()
                with open(file, "w+") as f:
                    f.write(content.decode(self.codec))
        except PermissionError as e:
            logger.error(f"{e.__class__.__name__}, Not enough access to modify the file, {e.strerror}")
            raise
        except UnicodeEncodeError as e:
            logger.error(f"{e.__class__.__name__}, Unicode symbol handling error, {e.start}-{e.end}\n"
                         f"encoding = {e.encoding}, object = {e.object}, reason = {e.reason}")
            raise
        except FileNotFoundError as e:
            logger.error(f"{e.__class__.__name__}, Not enough access to modify the file, {e.strerror}")
            raise
        except IsADirectoryError as e:
            logger.error(f"{e.__class__.__name__}, Not enough access to modify the file, {e.strerror}")
            raise
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, General handling error, {e.strerror}")
            raise
        else:
            logger.info(f"All files are encoded to {self.codec}")


@configure_custom_logging("recode_files")
def main():
    codec: str = input("Encoding:\n").lower().strip()
    logger.info(f"codec = {codec}")
    # codec: str = "cp1251"
    sentinel = ""
    paths = multiline_iterable_input(sentinel)
    logger.info(f"Paths to paths:\n{paths}")
    # paths = [str(item) for item in Path("C:\\Users\\tarasov-a\\Desktop\\Reports").iterdir() if item.suffix == ".txt"]

    file_path_factory: FilePathFactory = FilePathFactory([Path(path) for path in paths])
    files = file_path_factory.files
    text_file_recoder: TextFileRecoder = TextFileRecoder(codec, files)
    text_file_recoder.recode()
    for file in paths:
        with open(file, "r") as f:
            logger.error(f.encoding)


if __name__ == "__main__":
    main()
