import urllib.request
from enum import Enum
from typing import Optional, NamedTuple
from urllib.request import Request

from loguru import logger

from init_logger import configure_custom_logging, function_logging

_protocol = None


def set_default(is_secure: bool = True):
    if is_secure:
        globals()["_protocol"] = "HTTPS"
    else:
        globals()["_protocol"] = "HTTP"


class CustomPort(Enum):
    HTTP = 80
    HTTPS = 443

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomScheme(Enum):
    HTTP = "http"
    HTTPS = "https"

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class RequiredAttributeMissing(BaseException):
    """Required attribute has no specified value."""


class CustomHTTPRequest:
    def __init__(
            self, *,
            scheme: str = None,
            web_hook: str = None,
            host: str = None,
            port: int = None,
            params: tuple[tuple[str, str]] = None):
        if scheme is None:
            scheme = CustomScheme[f"{_protocol}"].value
        if port is None:
            port = CustomPort[f"{_protocol}"].value
        self._scheme: str = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._port: int = port
        self._params = params

    def __str__(self):
        return f"{self.__class__.__name__}: scheme = {self.scheme}, web hook = {self.web_hook}, host = {self.host}, " \
               f"post = {self.port}, params = {self.params}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.scheme}, {self.web_hook}, {self.host}, {self.port}, {self.params})>"

    def __hash__(self):
        return hash(self.url())

    @classmethod
    def __call__(cls):
        return cls()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.url() == other.url()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.url() != other.url()
        else:
            return NotImplemented

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, value):
        self._host = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value

    @property
    def scheme(self):
        return self._scheme

    @scheme.setter
    def scheme(self, value):
        self._scheme = value

    @property
    def web_hook(self):
        return self._web_hook

    @web_hook.setter
    def web_hook(self, value):
        self._web_hook = value

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, value):
        self._params = value

    def _get_params(self):
        if self.params is None:
            return ""
        _params: str = "&".join(map(lambda x, y: f"{x}={y}", ((x, y) for item in self.params for x, y in item)))
        return f"?{_params}"

    def _get_web_hook(self):
        if self.web_hook is None:
            logger.error("Web hook is a required attribute")
            raise RequiredAttributeMissing
        return self.web_hook.rstrip("/")

    def _get_scheme(self):
        if self.scheme is None:
            return "https"
        return self.scheme.lower().strip("/")

    def _get_port(self):
        if self.port is None:
            return CustomPort[f"{_protocol}"].value
        return self.port

    def _get_host(self):
        if self.host is None:
            logger.error("Host is a required attribute")
            raise RequiredAttributeMissing
        return self.host.strip("/")

    def url(self):
        return f"{self._get_scheme()}://{self._get_host()}:{self._get_port()}{self.web_hook}{self._get_params()}"


class CustomPreparedRequest(NamedTuple):
    url: str
    data: bytes
    headers: dict[str, str]
    host: str
    unverifiable: bool
    method: str

    def __str__(self):
        return f"{self.__class__.__name__}: method = {self.method}, host = {self.host}\n" \
               f"headers = {[header for header in self.headers]}\n" \
               f"url = {self.url}\n" \
               f"data = \"{self.data.decode('utf-8')}\""

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.url}, {self.data}, {[header for header in self.headers]}, " \
               f"{self.host}, {self.unverifiable}, {self.method})>"

    def __hash__(self):
        return hash((self.host, self.headers, self.data))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.to_request() == other.to_request()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.to_request() != other.to_request()
        else:
            return NotImplemented

    @classmethod
    def from_custom_request(
            cls,
            custom_http_request: CustomHTTPRequest,
            method: str = None,
            data: bytes = None,
            headers: dict[str, str] = None):
        if headers is None:
            headers = dict()
        if data is None:
            data = b''
        return cls(
            custom_http_request.url(),
            data,
            headers,
            custom_http_request.host,
            False,
            method)

    def to_request(self):
        if self.headers is None:
            self.headers = dict()
        return Request(self.url, self.data, self.headers, self.host, self.unverifiable, self.method)


class CustomHTTPClient:
    def __init__(self, _custom_prepared_request: CustomPreparedRequest):
        self._custom_prepared_request: CustomPreparedRequest = _custom_prepared_request
        self._request: Optional[Request] = self.custom_prepared_request.to_request()
        logger.info(self.custom_prepared_request)

    def __str__(self):
        return f"{self.__class__.__name__}: custom http request = {self._custom_prepared_request}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._custom_prepared_request})>"

    @classmethod
    def __call__(cls):
        return cls(CustomPreparedRequest.from_custom_request(CustomHTTPRequest()))

    @property
    def request(self):
        return self._request

    @request.setter
    def request(self, value):
        self._request = value

    @property
    def custom_prepared_request(self):
        return self._custom_prepared_request

    @custom_prepared_request.setter
    def custom_prepared_request(self, value):
        self._custom_prepared_request = value

    @function_logging
    def get_response(self):
        with urllib.request.urlopen(self.request) as req:
            return req.read().decode("utf-8")


@configure_custom_logging("send_request")
def main(is_secure: bool = True):
    set_default(is_secure)
    # noinspection PyTypeChecker
    custom_http_request: CustomHTTPRequest = CustomHTTPRequest(
        scheme=CustomScheme[f"{_protocol}"].value,
        host="dns.google",
        web_hook="/",
        port=CustomPort[f"{_protocol}"].value
    )
    custom_prepared_request: CustomPreparedRequest
    custom_prepared_request = CustomPreparedRequest.from_custom_request(custom_http_request, "GET")
    custom_http_client: CustomHTTPClient = CustomHTTPClient(custom_prepared_request)
    logger.info(custom_http_client.get_response())


if __name__ == "__main__":
    main()
