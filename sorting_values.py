from typing import Iterable


def sorting(values: Iterable[str]) -> list[str]:
    return sorted(values)


def parse_values(total_values: list[str], line: str):
    total_values.extend(line.split())
    return total_values


def main():
    sentinel: str = ""
    values: list[str] = list(iter(input, sentinel))
    total_values: list[str] = []
    for value in values:
        total_values = parse_values(total_values, value)
    print(sorting(total_values))


if __name__ == "__main__":
    main()
