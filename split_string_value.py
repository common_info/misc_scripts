def split(line: str, separator: str = None):
    return iter(line.split(separator))

def main():
    separator: str = ", "
    input_line: str = input().strip()
    for item in split(input_line, separator):
        print(item)


if __name__ == "__main__":
    main()
