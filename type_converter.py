from typing import Callable


class CustomConverter:
    _instance = None
    _from_type: str = None
    _to_type: str = None

    def __new__(cls, from_type: str = None, to_type: str = None):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            if from_type is not None:
                cls._from_type: str = from_type
            if to_type is not None:
                cls._to_type: str = to_type
        return cls._instance

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def hex_to_int(cls, value: str):
        if cls._from_type in ("hex", "int") and cls._to_type in ("hex", "int"):
            return int(value, base=16)
        else:
            return NotImplemented

    @classmethod
    def int_to_hex(cls, value: str):
        if cls._from_type in ("hex", "int") and cls._to_type in ("hex", "int"):
            return hex(int(value, base=10))
        else:
            return NotImplemented

    @classmethod
    def convert(cls, *args):
        return NotImplemented


class HexIntConverter(CustomConverter):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, "hex", "int")

    @classmethod
    def convert(cls, value: str):
        if value.startswith("0x"):
            return cls.hex_to_int(value)
        else:
            return cls.int_to_hex(value)


def main():
    values = input().lower().strip().split(" ")
    for value in values:
        print(f"{HexIntConverter().convert(value)}, {type(value)}")


if __name__ == "__main__":
    main()
