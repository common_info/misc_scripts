from typing import Callable, Iterable, Optional, Iterator


def prompt(text: Optional[Iterable[str]] = None):
    def proxy(func: Callable):
        def wrapper(*args, **kwargs):
            if text is not None:
                print(text)
            result = func(*args, **kwargs)
            return result
        return wrapper
    return proxy


@prompt("Input values:\n")
def multiline_iterator_input(sentinel: Optional[str] = None) -> Iterator[str]:
    if sentinel is None:
        sentinel = " "
    for item in iter(input, sentinel):
        yield item.strip()


@prompt("Input values:\n")
def multiline_iterable_input(sentinel: Optional[str] = None) -> tuple[str, ...]:
    if sentinel is None:
        sentinel = ""
    return tuple(multiline_iterator_input(sentinel))


@prompt("input values:\n")
def multiline_string_input(sentinel: Optional[str] = None) -> str:
    if sentinel is None:
        sentinel = ""
    return "\n".join(item for item in iter(input, sentinel))


if __name__ == "__main__":
    multiline_string_input()
