import re
from enum import Enum
from re import Match
from typing import NamedTuple, Iterable, Union

from loguru import logger


class InvalidIpAddressOctetError(ValueError):
    """All octets must be in range 0-255."""


class LocalZoneSystemNotFoundError(KeyError):
    """The system is not found at the zone."""


class LocalZoneSystemObjectTypeError(TypeError):
    """The system must be of the ProteiSystem or str type."""


class LocalZoneSystemNameTypeError(TypeError):
    """The system name must be of the str type."""


class LocalZoneDictItemTypeError(TypeError):
    """The local zone dict item must be of the LocalZone type."""


class LocalZoneDictItemNotFoundError(KeyError):
    """The local zone is not found at the dict."""


class SystemInstance(Enum):
    CORE = "core"
    WEB = "web"
    DATABASE = "database"
    API = "api"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.name})>"


class IPv4Address(NamedTuple):
    octet_1: int = 127
    octet_2: int = 0
    octet_3: int = 0
    octet_4: int = 1
    port: int = 0

    def validate(self):
        for attr in self._fields:
            if getattr(self, attr) < 0 or getattr(self, attr) > 255:
                logger.error("Invalid IP address")
                raise InvalidIpAddressOctetError

    def __str__(self):
        _port_string = f"" if self.port == 0 else f":{self.port}"
        return f"{self.octet_1}.{self.octet_2}.{self.octet_3}.{self.octet_4}{_port_string}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"

    @staticmethod
    def _pattern():
        return re.compile(r"(\d+)\.(\d+)\.(\d+)\.(\d+):?(\d+)?")

    @classmethod
    def from_ip(cls, ip: str):
        match: Match = re.match(cls._pattern(), ip)
        if match.group(5):
            return cls(
                int(match.group(1)),
                int(match.group(2)),
                int(match.group(3)),
                int(match.group(4)),
                int(match.group(5)))
        else:
            return cls(
                int(match.group(1)),
                int(match.group(2)),
                int(match.group(3)),
                int(match.group(4)))



class ProteiSystem(NamedTuple):
    name: str
    instance: SystemInstance

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}, {self.instance.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.name}, {self.instance.name}>"


class LocalZone:
    def __init__(self, ip_address: IPv4Address, protei_systems: Iterable[ProteiSystem] = None):
        if protei_systems is None:
            protei_systems: set[ProteiSystem] = set()
        self._ip_address: IPv4Address = ip_address
        self._protei_systems: set[ProteiSystem] = {*protei_systems}

    @property
    def systems(self):
        return {protei_system.name for protei_system in self._protei_systems}

    def __str__(self):
        return f"{self.ip}: {self.systems}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self._ip_address)}, {self.systems})>"

    @property
    def ip_address(self):
        return self._ip_address

    @ip_address.setter
    def ip_address(self, value):
        self._ip_address = value

    @property
    def ip(self):
        return str(self._ip_address)

    @property
    def protei_systems(self):
        return self._protei_systems

    @protei_systems.setter
    def protei_systems(self, value):
        self._protei_systems = value

    def __hash__(self):
        return hash(self.ip)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.ip == other.ip
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.ip != other.ip
        else:
            return NotImplemented

    def __iter__(self):
        return iter(self.systems)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.systems
        elif isinstance(item, ProteiSystem):
            return item in self._protei_systems
        else:
            return False

    def __add__(self, other):
        if isinstance(other, ProteiSystem):
            self._protei_systems.add(other)
        else:
            logger.error(f"The system name of the str type is expected, but {type(other)} received")
            raise LocalZoneSystemObjectTypeError

    __radd__ = __add__
    __iadd__ = __add__

    def __sub__(self, other):
        if not isinstance(other, (ProteiSystem, str)):
            logger.error(f"The system of the ProteiSystem or str type is expected, but {type(other)} received")
            raise LocalZoneSystemObjectTypeError
        if isinstance(other, str):
            self._protei_systems.remove(self.__getitem__(other))
            return
        if other in self:
            self._protei_systems.remove(other)
        else:
            logger.error(f"The unknown system {other}")
            raise LocalZoneSystemNotFoundError

    __isub__ = __sub__

    def _get_protei_system(self, item):
        for protei_system in self._protei_systems:
            if protei_system.name == item:
                return protei_system

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self:
                return self._get_protei_system(item)
            else:
                logger.error(f"The unknown system {item}")
                raise LocalZoneSystemNotFoundError
        else:
            logger.error(f"The invalid system name type {type(item)}")
            raise LocalZoneSystemNameTypeError

    def __len__(self):
        return len(self.systems)

    def __bool__(self):
        return len(self) > 0

    def get_type(self, system: str):
        if system in self:
            return self.__getitem__(system).instance.value


class LocalZoneDict(dict):
    def __init__(self, local_zones: Iterable[LocalZone] = None):
        super().__init__()
        if local_zones is None:
            local_zones: tuple[LocalZone, ...] = ()
        for local_zone in local_zones:
            self.__setitem__(local_zone.ip, local_zone)

    def __call__(self, **kwargs):
        return self(**kwargs)

    def all_zones_system(self, system_name: str) -> list[str]:
        k: str
        v: LocalZone
        return [k for k, v in self.items() if system_name in v.systems]

    def get(self, __key: str) -> LocalZone | None:
        return super().get(__key)

    def add_protei_system(self, ip: str, protei_system: ProteiSystem):
        self.get(ip) + protei_system

    def delete_protei_system(self, ip: str, protei_system: Union[str, ProteiSystem]):
        self.get(ip) - protei_system

    def __add__(self, other):
        if isinstance(other, LocalZone):
            self.__setitem__(other.ip, other)
        else:
            logger.error(f"The local zone of the LocalZone type is expected, but {type(other)} received")
            raise LocalZoneDictItemTypeError

    __radd__ = __add__
    __iadd__ = __add__

    def __sub__(self, other):
        if not isinstance(other, (LocalZone, str)):
            logger.error(f"The local zone of the LocalZone or str type is expected, but {type(other)} received")
            raise LocalZoneDictItemTypeError
        if isinstance(other, str):
            self.__delitem__(other)
            return
        if other in self.values():
            self.__delitem__(other.ip)
        else:
            logger.error(f"The unknown local zone {other}")
            raise LocalZoneSystemNotFoundError
